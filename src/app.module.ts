import { Module } from '@nestjs/common';
//import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm'
import { GraphQLModule } from '@nestjs/graphql'
import { GraphqlModule } from './config/graphql/graphql.module'
import { GraphqlService } from './config/graphql/graphql.service'
import { TypeormModule } from './config/typeorm/typeorm.module'
import { TypeormService } from './config/typeorm/typeorm.service'
import { UserModule } from './modules/user/user.module'
import { WinstonModule } from 'nest-winston'
import { AccountModule } from './modules/account/account.module';
import { ShipperModule } from './modules/shipper/shipper.module';
import { RoleModule } from './modules/role/role.module';
import { ShopModule } from './modules/shop/shop.module';
import { MenuModule } from './modules/menu/menu.module';
import { DishModule } from './modules/dish/dish.module';
import * as winston from 'winston'
import { ImageModule } from './modules/image/image.module';
import { OrderModule } from './modules/order/order.module';
import { CouponModule } from './modules/coupon/coupon.module';

const {
	combine,
	json,
	timestamp,
	label,
	printf,
	prettyPrint,
	colorize
} = winston.format

@Module({
  imports: [
    GraphQLModule.forRootAsync({
    useClass: GraphqlService,
  }),
  TypeOrmModule.forRootAsync({
    useClass: TypeormService
  }),WinstonModule.forRootAsync({
    useFactory: () => ({
      // options
      format: combine(
        label({ label: '🥢 delivery!' }),
        json(),
        timestamp(),
        // prettyPrint(),
        // colorize(),
        printf(({ level, message, label, timestamp }) => {
          console.log(level)
          return `{\n\tlabel: ${label},\n\ttimestamp: ${timestamp},\n\tlevel: ${level},\n\tmessage: ${message}\n},`
        })
      ),
      transports: [
        new winston.transports.Console(),
        new winston.transports.File({
          filename: 'src/logs/combined.log',
          level: 'info'
        }),
        new winston.transports.File({
          filename: 'src/logs/errors.log',
          level: 'error'
        })
      ],
      exceptionHandlers: [
        new winston.transports.Console(),
        new winston.transports.File({
          filename: 'src/logs/exceptions.log'
        })
      ]
    }),
    inject: []
  }),
  GraphqlModule,
  TypeormModule,
  UserModule,
  AccountModule,
  RoleModule,
  ShopModule,
  MenuModule,
  DishModule,
  ShipperModule,
  ImageModule,
  OrderModule,
  CouponModule,
]
})
export class AppModule {}
