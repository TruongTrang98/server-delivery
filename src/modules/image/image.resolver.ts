import {
	Resolver,
	Mutation,
    Args,
} from '@nestjs/graphql'
import { ImageService } from './image.service'
import { ImageInput } from './image.entity'

@Resolver('Image')
export class ImageResolver {
	constructor(
		private readonly imageService: ImageService
    ) {}

    @Mutation (() => String)
    async updateImage(@Args('_id')_id: string, @Args('image')image: ImageInput){
        return await this.imageService.updateImage(_id, image)
    }
}
