import { Module } from '@nestjs/common';
import { ImageService } from './image.service'
import { ImageResolver } from './image.resolver';

@Module({
    imports: [],
	providers: [ImageService, ImageResolver],
    exports: [ImageService]
})
export class ImageModule{}
