import { Injectable } from '@nestjs/common'
import { ApolloError } from 'apollo-server-core'
import * as fs from 'fs'
import { ImageInput } from './image.entity'

@Injectable()
export class ImageService {
	constructor() {}

    async updateImage (name: string, image: ImageInput): Promise<String> {
        const file = image.data.replace(/^data:image\/(png|gif|jpeg);base64,/, '')
                await fs.writeFile(
                    `static/images/${name}.${image.type.split('/')[1]}`,
                    file,
                    'base64',
                    err => {
                    if (err) {
                        throw new ApolloError(err.message, '403')
                    }
                    }
                )
        return '200'
    }

}