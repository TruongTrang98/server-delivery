import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Dish } from './dish.entity';
import { DishService } from './dish.service';
import { DishResolver } from './dish.resolver';
import { Shop } from '../shop/shop.entity';
import { ShopModule } from '../shop/shop.module';
import { ImageModule } from '../image/image.module';

@Module({
    imports: [TypeOrmModule.forFeature([Dish]), ShopModule, ImageModule],
    providers: [DishService, DishResolver],
    exports: [DishService]
})
export class DishModule {}
