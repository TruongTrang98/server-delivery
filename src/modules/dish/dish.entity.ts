import {
    Entity,
    ObjectIdColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    BeforeInsert,
} from 'typeorm'
import * as uuid from 'uuid'
import { Shop } from '../shop/shop.entity'
import { ImageInput } from '../image/image.entity'
import { Coupon } from '../coupon/coupon.entity'

@Entity()
export class Dish {
    @ObjectIdColumn()
    _id: string

    @Column()
    name: string

    @Column()
    imageUrl: string

    @Column()
    price: number

    @Column()
    couponId: string

    @Column()
    shopId: string

    @Column()
    isActive: boolean

    @CreateDateColumn({ type: 'timestamp with time zone' })
    createdAt: string

    @UpdateDateColumn({ type: 'timestamp with time zone' })
    updatedAt: string

    @BeforeInsert()
	async beforeInsert() {
		this._id = await uuid.v4()
        this.isActive = true
	}
}

export class DishInput {
    name: string
    image: ImageInput
    price: number
}

export class UpdateDishInput{
    name: string
    imageUrl: string
    price: number
    couponId: string
}

export class DishResponse {
    _id: string
    name: string
    imageUrl: string
    price: number
    coupon: Coupon
    shop: Shop
    isActive: boolean
    createdAt: string
    updatedAt: string
}