import { Resolver, Query, Mutation, Args, Context } from '@nestjs/graphql';
import { DishService } from './dish.service';
import { DishResponse, Dish, DishInput } from './dish.entity';

@Resolver('Dish')
export class DishResolver {
    constructor(
        private readonly dishService: DishService,
    ) { }
    
    @Query(() => [DishResponse])
    async dishes(@Context('currentUser')currentUser) {
        return await this.dishService.dishes(currentUser)
    }

    @Query(() => [DishResponse])
    async dishesWithoutCoupon(@Context('currentUser')currentUser) {
        return await this.dishService.dishesWithoutCoupon(currentUser)
    }

    @Query(() => [DishResponse])
    async dishesForCoupon(@Context('currentUser')currentUser) {
        return await this.dishService.dishesForCoupon(currentUser)
    }

    // @Query(() => [DishResponse])
    // async dishesForUpdateCoupon(@Context('currentUser')currentUser) {
    //     return await this.dishService.dishesForUpdateCoupon(currentUser)
    // }

    // @Query(() => [DishResponse])
    // async dishesForUser() {
    //     return await this.dishService.dishesForUser()
    // }

    @Query(() => DishResponse)
    async dishById(@Args('_id') _id: string) {
        return await this.dishService.dishById(_id)
    }

    @Mutation(() => String)
    async createDish(@Context('currentUser')currentUser, @Args('input') input: DishInput) {
        return await this.dishService.createDish(currentUser, input);
    }

    @Mutation(() => String)
    async updateDish(@Context('currentUser')currentUser, @Args('_id') _id: string, @Args('input') input: DishInput) {
        return await this.dishService.updateDish(currentUser, _id, input);
    }

    @Mutation(() => String)
    async deleteDish(@Context('currentUser')currentUser, @Args('_id') _id: string) {
        return await this.dishService.deleteDish(currentUser, _id);
    }
}
