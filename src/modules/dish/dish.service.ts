import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Dish, DishInput, DishResponse, UpdateDishInput } from './dish.entity';
import { MongoRepository, getMongoRepository } from 'typeorm';
import { ApolloError } from 'apollo-server-core';
import * as diacritic from 'diacritic'
import { Shop } from '../shop/shop.entity';
import * as fs from 'fs'
import { ImageService } from '../image/image.service';

@Injectable()
export class DishService {
    constructor(
        @InjectRepository(Dish)
        private readonly dishRepository: MongoRepository<Dish>,
        private readonly imageService: ImageService
    ) { }
    
    async dishes(currentShop): Promise<DishResponse[]> {
        // const shop = await this.shopService.findByAccountId(accountId);
        // const shopId = shop._id;
        // console.log(accountId)
        console.log(currentShop)
        const dishes = await this.dishRepository.aggregate([
            {
                $match: {
                    shopId: currentShop._id
                }
            },
            {
                $lookup: {
                    from: "shop",
                    localField: 'shopId',
                    foreignField: "_id",
                    as: "shop"
                }
            },
            {
                $unwind: "$shop"
            },
            {
                $lookup: {
                    from: "coupon",
                    localField: 'couponId',
                    foreignField: "_id",
                    as: "coupon"
                }
            },
            {
                $unwind: {
                    path: "$coupon",
                    "preserveNullAndEmptyArrays": true
                },
            }
        ]).toArray()
        return dishes
    }

    async dishesWithoutCoupon(currentShop): Promise<DishResponse[]> {
        // const shop = await this.shopService.findByAccountId(accountId);
        // const shopId = shop._id;
        // console.log(accountId)
        console.log(currentShop)
        const dishesWithoutCoupon = await this.dishRepository.aggregate([
            {
                $match: {
                    $and: [
                        {shopId: currentShop._id},
                        {couponId: null}
                    ]
                }
            }
        ]).toArray()
        return dishesWithoutCoupon
    }

    // // CHeck if dish have coupon => reject
    async dishesForCoupon(currentShop): Promise<DishResponse[]> {
        const dishes = await this.dishRepository.aggregate([
            {
                $match: {
                    shopId: currentShop._id
                }
            },
            {
                $lookup: {
                    from: "shop",
                    localField: 'shopId',
                    foreignField: "_id",
                    as: "shop"
                }
            },
            {
                $unwind: "$shop"
            },
            {
                $lookup: {
                    from: "coupon",
                    localField: 'couponId',
                    foreignField: "_id",
                    as: "coupon"
                }
            },
            {
                $unwind: {
                    path: "$coupon",
                    "preserveNullAndEmptyArrays": true
                },
            }
        ]).toArray()
        const dishesForCoupon = dishes.filter(dish => dish.couponId == null)
        return dishesForCoupon
    }

    async dishesForUpdateCoupon(currentShop): Promise<DishResponse[]> {
        const dishes = await this.dishRepository.aggregate([
            {
                $match: {
                    shopId: currentShop._id
                }
            },
            {
                $lookup: {
                    from: "shop",
                    localField: 'shopId',
                    foreignField: "_id",
                    as: "shop"
                }
            },
            {
                $unwind: "$shop"
            },
            {
                $lookup: {
                    from: "coupon",
                    localField: 'couponId',
                    foreignField: "_id",
                    as: "coupon"
                }
            },
            {
                $unwind: {
                    path: "$coupon",
                    "preserveNullAndEmptyArrays": true
                },
            }
        ]).toArray()
        const dishesForCoupon = dishes.filter(dish => dish.couponId == null)
        return dishesForCoupon
    }


    // async dishesForUser(): Promise<DishResponse[]> {
    //     const dishes = await this.dishRepository.aggregate([
    //         {
    //             $lookup: {
    //                 from: "shop",
    //                 localField: 'shopId',
    //                 foreignField: "_id",
    //                 as: "shop"
    //             }
    //         },
    //         {
    //             $unwind: "$shop"
    //         }
    //     ]).toArray()
    //     return dishes
    // }

    async dishById(_id: string): Promise<DishResponse> {
        const dishResponse = await this.dishRepository.aggregate([
            {
                $match: {
                    _id
                }
            },
            {
                $lookup: {
                    from: "shop",
                    localField: 'shopId',
                    foreignField: "_id",
                    as: "shop"
                }
            },
            {
                $unwind: "$shop"
            }
        ]).toArray()
        return dishResponse[0];
    }

    async createDish(currentShop, input: DishInput): Promise<String> {
        // const shop = await this.shopService.findByAccountId(currentShop._id)
        // const shopId = shop._id
        try {
            const { name, image, price } = input
           // const file = image.data.replace(/^data:image\/(png|gif|jpeg);base64,/, '')
            // await fs.writeFile(
            //     `static/images/${currentShop._id}-${Date.now()}.${image.type.split('/')[1]}`,
            //     file,
            //     'base64',
            //     err => {
            //         if (err) {
            //             console.log(err)
            //         }
            //     }
            // )
            const dish = new Dish()
            dish.name = name
            dish.imageUrl = `${currentShop._id}-${Date.now()}.${image.type.split('/')[1]}`
            dish.price = price
            dish.shopId = currentShop._id
            await this.dishRepository.save(dish)
            await this.imageService.updateImage(dish.imageUrl.split('.')[0], image)


            const shop = await getMongoRepository(Shop).findOne({ _id: currentShop._id })

            const allDishesName = (await this.dishes(currentShop._id))
                                .map(dish => dish.name)
            shop.dishesStr = diacritic.clean(allDishesName.join(' ')).toLowerCase()
            await getMongoRepository(Shop).save(shop)

            return '200';
            // const { name, imageUrl, price } = input
            // const newDish = this.dishRepository.create({ name, imageUrl, price, shopId: currentShop._id })
            // if (newDish) {
            //     await this.dishRepository.save(newDish);
            //     return '200';
            // // }
        } catch (error) {
            throw new ApolloError(
                error,
                error.extensions ? error.extensions.code : '403'
            )
        }
    }

    async updateDish(currentShop, _id: string, input: DishInput): Promise<String> {
        try {
            //Check đúng shop
            // const shop = await this.shopService.findByAccountId(accountId)
            // const shopId = shop._id
            const { name, image, price } = input;
            const existDish = await this.dishRepository.findOne({ _id })
            // console.log(checkExisted);
            if(existDish.shopId != currentShop._id) return '409'
            // const { name: oldName, imageUrl: oldImageUrl, price: oldPrice } = checkExisted;
            if (!existDish) {
                return '409';
            }
            // console.log(price, oldPrice);
            // const updName = name != "" ? name : oldName;
            // const updImageUrl = imageUrl != "" ? imageUrl : oldImageUrl;
            // const updPrice = price != null ? price : oldPrice;
            // console.log(updName, updImageUrl, updPrice);
            existDish.name = name
            existDish.price = price
            if (image.data) {
                existDish.imageUrl = `${existDish._id}.${image.type.split('/')[1]}`
                await this.imageService.updateImage(existDish.imageUrl.split('.')[0], image)
            }
            await this.dishRepository.save(existDish)

            const shop = await getMongoRepository(Shop).findOne({ _id: currentShop._id })

            const allDishesName = (await this.dishes(currentShop))
                                .map(dish => dish.name)
            shop.dishesStr = diacritic.clean(allDishesName.join(' ')).toLowerCase()
            await getMongoRepository(Shop).save(shop)

            return '200'
        }
        catch (error) {
            throw new ApolloError(
                error,
                error.extensions ? error.extensions.code : '403'
            )
        }
    }

    async updateDishCoupon(currentShopId, _id: string, input: UpdateDishInput): Promise<String> {
        try {
            //Check đúng shop
            // const shop = await this.shopService.findByAccountId(accountId)
            // const shopId = shop._id
            const { name, imageUrl, price, couponId } = input;
            const checkExisted = await this.dishRepository.findOne({ _id })
            console.log(checkExisted);
            if(checkExisted.shopId != currentShopId) return '409 lan 1'
            const { name: oldName, imageUrl: oldImageUrl, price: oldPrice, couponId: oldCouponId } = checkExisted;
            if (!checkExisted) {
                return '409 lan 2';
            }
            // console.log(price, oldPrice);
            const updName = name != "" ? name : oldName;
            const updImageUrl = imageUrl != "" ? imageUrl : oldImageUrl;
            const updPrice = price != null ? price : oldPrice;
            const udpCoupon = couponId != "" ? couponId : oldCouponId;
            // console.log(updName, updImageUrl, updPrice);
            await this.dishRepository.save({ _id, name: updName, imageUrl: updImageUrl, price: updPrice, couponId: udpCoupon })
            return '200'
        }
        catch (error) {
            throw new ApolloError(
                error,
                error.extensions ? error.extensions.code : '403'
            )
        }
    }

    async deleteCouponIdInDish(currentShopId, _id: string): Promise<String> {
        try {
            const checkExisted = await this.dishRepository.findOne({ _id })
            console.log(checkExisted);
            if(checkExisted.shopId != currentShopId) return '409 lan 1'
            if (!checkExisted) {
                return '409 lan 2';
            }
            await this.dishRepository.save({ _id, checkExisted, couponId: null})
            return '200'
        }
        catch (error) {
            throw new ApolloError(
                error,
                error.extensions ? error.extensions.code : '403'
            )
        }
    }

    async addCouponIdToDish(currentShopId, _id: string, couponId: string): Promise<String> {
        try {
            const checkExisted = await this.dishRepository.findOne({ _id })
            console.log(checkExisted);
            if(checkExisted.shopId != currentShopId) return '409 lan 1'
            if (!checkExisted) {
                return '409 lan 2';
            }
            await this.dishRepository.save({ _id, checkExisted, couponId: couponId})
            return '200'
        }
        catch (error) {
            throw new ApolloError(
                error,
                error.extensions ? error.extensions.code : '403'
            )
        }
    }

    async deleteDish(currentShop, _id: string): Promise<String> {
        try {
            //Check đúng shop
            // const shop = await this.shopService.findByAccountId(accountId)
            // const shopId = shop._id
            const existedDish = await this.dishRepository.findOne({ _id })
            if(existedDish.shopId != currentShop._id) return '409'
            if (!existedDish) {
                return '409'
            }
            await this.dishRepository.deleteOne({ _id })
            return '200'
        }
        catch (error) {
            throw new ApolloError(
                error,
                error.extensions ? error.extensions.code : '403'
            )
        }
    }
}