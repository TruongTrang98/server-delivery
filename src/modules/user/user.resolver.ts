import {
	Resolver,
	Query,
	Mutation,
	Args,
	Subscription,
	Context,
} from '@nestjs/graphql'
import { UserService } from './user.service'
import {
	 UserResponse, UserInput,
} from './user.entity'
import { ImageInput } from '../image/image.entity'

@Resolver('User')
export class UserResolver {
	constructor(private readonly userService: UserService) {}

	@Query(() => [UserResponse])
	async users(
		@Args('txtSearch') txtSearch: string, 
		@Args('txtFilter') txtFilter: string, 
		@Args('offset') offset: number, 
		@Args('limit') limit: number
		) {
		return await this.userService.findAll({ txtSearch, txtFilter, offset, limit })
	}

	@Query(() => Number)
	async numberOfUser(@Args('txtSearch') txtSearch: string, @Args('txtFilter') txtFilter: string) {
		return await this.userService.numberOfUser(txtSearch, txtFilter)
	}

	// @Query(() => [UserResponse])
	// async users(@Args('name') name: string, @Args('isLocked') isLocked: boolean) {
	// 	return await this.userService.findAll(name, isLocked)
	// }

	@Query(() => UserResponse)
	async userById(@Args('_id')_id: string) {
		return await this.userService.findById(_id)
	}
	@Query(() => UserResponse)
	async me (@Context('currentUser') currentUser){
		return await this.userService.findById(currentUser._id)
	}

	@Mutation(() => String)
	async createUser(
		@Args('input') input: UserInput,
		//@Context('pubSub') pubSub
	) {
		const createdUser = await this.userService.create(input)
		
		//pubSub.publish('userCreated', { userCreated: createdUser })
		return createdUser
	}

	@Mutation(() => String)
	async updateUserInfo(
		@Args('_id') _id: string,
		@Args('input') input: UserInput
	) {
		return await this.userService.updateInfo(_id, input)
	}

	@Mutation(() => String)
	async updateUserAvatar(
		@Args('_id') _id: string,
		@Args('image') image: ImageInput
	) {
		return await this.userService.updateAvatar(_id, image)
	}
	
	// @Subscription()
	// async userCreated(@Context('pubSub') pubSub: any) {
	// 	return await pubSub.asyncIterator('userCreated')
	// }
}
