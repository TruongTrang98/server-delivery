import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './user.entity';
import { UserResolver } from './user.resolver';
import { UserService } from './user.service';
import { AccountModule } from '../account/account.module';
import { ImageModule } from '../image/image.module';

@Module({
    imports: [TypeOrmModule.forFeature([User]), AccountModule, ImageModule],
	providers: [UserResolver, UserService],
    exports: [UserService]
})
export class UserModule{}
