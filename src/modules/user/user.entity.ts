import {
	Entity,
	ObjectIdColumn,
	Column,
	CreateDateColumn,
	UpdateDateColumn,
	BeforeInsert,
	BeforeUpdate
} from 'typeorm'
import * as uuid from 'uuid'
import * as bcrypt from 'bcrypt'
import {
	IsString,
	IsNotEmpty,
	Length,
	MinLength,
	IsBoolean
} from 'class-validator'
import { Account } from '../account/account.entity'
import { ImageInput } from '../image/image.entity'

export class UserInput {
	name: string
	image: ImageInput
	gender: string
	email: string
	phoneNumber: string
	address: string
	password: string
}

export class UserResponse{
	_id: string
	name: string
	imageUrl: string
	gender: string
	address: string
	email: string
	phoneNumber: string
	account: Account
	isActive: boolean
	createdAt: string
	updatedAt: string
}


@Entity()
export class User {
	@ObjectIdColumn()
	_id: string

	@Column()
	@IsString()
	@IsNotEmpty()
	name: string

	@Column()
	@IsString()
	imageUrl: string

	@Column()
	gender: string

	@Column()
	@IsString()
	@IsNotEmpty()
	email: string

	@Column()
	@IsString()
	@IsNotEmpty()
	phoneNumber: string

	@Column()
	@IsString()
	@IsNotEmpty()
	address: string

	@Column()
	@IsString()
	@IsNotEmpty()
	accountId: string

	@Column()
	@IsString()
	@IsNotEmpty()
	isActive: boolean


	@CreateDateColumn()
	createdAt: string

	@UpdateDateColumn()
	updatedAt: string

	@BeforeInsert()
	async b4register() {
		const uid = await uuid.v4()
		this._id = uid
		this.isActive = true
	}
}
