import { Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { MongoRepository } from 'typeorm'
import { ApolloError } from 'apollo-server-core'
import {
	User,
	UserResponse,
	UserInput
} from './user.entity'
import { AccountService } from '../account/account.service'
import { ImageService } from '../image/image.service'
import { ImageInput } from '../image/image.entity'

@Injectable()
export class UserService {
	constructor(
		@InjectRepository(User)
		private readonly userRepository: MongoRepository<User>,
		private readonly accountService: AccountService,
		private readonly imageService: ImageService
	) { }

	async findAll({ txtSearch, txtFilter, offset, limit }): Promise<UserResponse[]> {
		const users = await this.userRepository.aggregate([
			{
				$lookup: {
					from: "account",
					localField: "accountId",
					foreignField: "_id",
					as: "account"
				}
			},
			{
				$unwind: "$account"
			},
			{
				$match: {
					name: { $regex: !!txtSearch ? `${txtSearch}` : `` },
					$or: (txtFilter === 'all' || !txtFilter) ? [{ "account.isLocked": false }, { "account.isLocked": true }] 
						: txtFilter === 'isLocked' ? [{ "account.isLocked": true }]
						: [{ "account.isLocked": false }]
				}
			},
			{
                $skip: +offset
            },
            {
                $limit: +limit
			}
		]).toArray()

		return users;
	}

	async numberOfUser (txtSearch: string, txtFilter: string): Promise<number> {
		return (await this.userRepository.aggregate([
			{
				$lookup: {
					from: "account",
					localField: "accountId",
					foreignField: "_id",
					as: "account"
				}
			},
			{
				$unwind: "$account"
			},
			{
				$match: {
					name: { $regex: !!txtSearch ? `${txtSearch}` : `` },
					$or: (txtFilter === 'all' || !txtFilter) ? [{ "account.isLocked": false }, { "account.isLocked": true }] 
						: txtFilter === 'isLocked' ? [{ "account.isLocked": true }]
						: [{ "account.isLocked": false }]
				}
			}
		]).toArray()).length
	}

	// for filter (update)
	// async findAll(name: string, isLocked: boolean): Promise<UserResponse[]> {
	// 	const users = await this.userRepository.aggregate([
	// 		{
	// 			$lookup: {
	// 				from: "account",
	// 				localField: "accountId",
	// 				foreignField: "_id",
	// 				as: "account"
	// 			}
	// 		},
	// 		{
	// 			$unwind: "$account"
	// 		},
	// 		{
	// 			$match: {"account.isLocked": isLocked}
	// 		}
	// 	]).toArray()

	// 	return users;
	// }

	async findById(_id: string): Promise<UserResponse> {
		const userResponse = await this.userRepository.aggregate([
			{
				$match: {
					_id
				}
			},
			{
				$lookup: {
					from: "account",
					localField: "accountId",
					foreignField: "_id",
					as: "account"
				}
			},
			{
				$unwind: "$account"
			}
		]).toArray()
		return userResponse[0];
	}

	async create(input: UserInput): Promise<String> {
		try {
			const { name, email, address, password, phoneNumber, gender, image } = input

			const existedUser = await this.userRepository.findOne({ email })

			if (existedUser) {
				return '410'
			}
			const account = await this.accountService.create({ username: email, password, role: "user" })
			
			if (account) {		
				const user = new User()
				user.accountId = account._id
				user.imageUrl = image ? `${account._id}.${image.type.split('/')[1]}` : null
				user.gender = gender
				user.address = address
				user.email = email
				user.phoneNumber = phoneNumber
				user.name = name
				await this.userRepository.save(user)
				if(image) {
					await this.imageService.updateImage(account._id, image)
				}
				return '200'
			} else {
				return '408'
			}
		} catch (error) {
			throw new ApolloError(
				error,
				error.extensions ? error.extensions.code : '403'
			)
		}
	}

	async updateInfo(_id: string, input: UserInput): Promise<String> {
		try {
			delete input.password
			delete input.image
			const user = await this.userRepository.findOne({ _id })
			if (!user) {
				return '409'
			}
			await this.userRepository.save({ _id, ...input, imageUrl: user.imageUrl  })
			return '200'
		} catch (error) {
			throw new ApolloError(
				error,
				error.extensions ? error.extensions.code : '403'
			)
		}
	}

	async updateAvatar(_id: string, image: ImageInput): Promise<String> {
		try {
			let user = await this.userRepository.findOne({ _id })
			if(!user) {
				return '409'
			}
			user.imageUrl = `${_id}.${image.type.split('/')[1]}`
			await this.userRepository.save(user)
			await this.imageService.updateImage(_id, image)
			return '200'
		} catch (error) {
			throw new ApolloError(
				error,
				error.extensions ? error.extensions.code : '403'
			)
		}
	}

}
