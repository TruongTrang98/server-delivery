import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Order } from './order.entity';
import { OrderService } from './order.service';
import { OrderResolver } from './order.resolver';
import { ShopModule } from '../shop/shop.module';

@Module({
    imports: [TypeOrmModule.forFeature([Order]), ShopModule],
	providers: [OrderService, OrderResolver],
    exports: [OrderService]
})
export class OrderModule{}
