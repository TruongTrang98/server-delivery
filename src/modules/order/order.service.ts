import { Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { MongoRepository } from 'typeorm'
import { Order, OrderResponse, OrderInput } from './order.entity'
import { ApolloError } from 'apollo-server-core'

@Injectable()
export class OrderService {
	constructor(
		@InjectRepository(Order)
		private readonly orderRepository: MongoRepository<Order>,
	) {}

	// Những đơn hàng chuyển sang status shipperRecerveFood, shop hoàn thành nv nên done
	async findAllShopDoneStatus(currentShop, idSearch: string): Promise<OrderResponse[]>{
		const orderResponse = await this.orderRepository.aggregate([
			{
				$match: {
					shopId: currentShop._id,
					$or: [{status: 'done'}, {status: 'shipperReceiveFood'}],
					_id: { $regex: !!idSearch ? `${idSearch}` : `` }
				}
			},
			{
			  $lookup: {
				from: "user",
				localField: "userId",
				foreignField: "_id",
				as: "user"
			  }
			},
			{
				$unwind:"$user"
			},
			{
				$lookup: {
				  from: "shop",
				  localField: "shopId",
				  foreignField: "_id",
				  as: "shop"
				}
			},
			{
				$unwind:"$shop"
			},
		  ]).toArray();
		return orderResponse
	}
	
	async findAllPendingStatus(currentShop): Promise<OrderResponse[]>{
		// const currentShop = await this.shopService.findById(currentUser._id)
		const orderResponse = await this.orderRepository.aggregate([
			{
				$match: {
				  	shopId: currentShop._id,
					status: 'pending'
				}
			},
			{
			  $lookup: {
				from: "user",
				localField: "userId",
				foreignField: "_id",
				as: "user"
			  }
			},
			{
				$unwind:"$user"
			},
			{
				$lookup: {
				  from: "shop",
				  localField: "shopId",
				  foreignField: "_id",
				  as: "shop"
				}
			},
			{
				$unwind:"$shop"
			},
		  ]).toArray();
		return orderResponse
	}

	async findAllReceivedAndShipperReceivedStatus(currentShop, idSearch: string): Promise<OrderResponse[]>{
		// const currentShop = await this.shopService.findByAccountId(_id)
		const orderResponse = await this.orderRepository.aggregate([
			{
				$match: {
				  	shopId: currentShop._id,
					$or: [{ status: 'shopReceiveOrder' }, { status: 'shipperReceiveOrder' }],
					_id: { $regex: !!idSearch ? `${idSearch}` : `` }
				}
			},
			{
			  $lookup: {
				from: "user",
				localField: "userId",
				foreignField: "_id",
				as: "user"
			  }
			},
			{
				$unwind:"$user"
			},
			{
				$lookup: {
				  from: "shop",
				  localField: "shopId",
				  foreignField: "_id",
				  as: "shop"
				}
			},
			{
				$unwind:"$shop"
			},
		  ]).toArray();
		return orderResponse
	}

	async findByUserId(_id: string): Promise<OrderResponse[]>{
		const orderResponse = await this.orderRepository.aggregate([
			{
			  $match: {
				  userId: _id
			  }
			},
			{
			  $lookup: {
				from: "user",
				localField: "userId",
				foreignField: "_id",
				as: "user"
			  }
			},
			{
				$lookup: {
				  from: "shop",
				  localField: "shopId",
				  foreignField: "_id",
				  as: "shop"
				}
			  },
			{
			  $unwind: "$user"
			}
		  ]).toArray();
		return orderResponse[0]
	}

	async book (input: OrderInput): Promise<OrderResponse>{
		const { shopId, userId, dishes, address, name, phoneNumber, costDishes, costShip } = input

		let order = new Order()
		order.shopId = shopId
		order.userId = userId
		order.dishes = dishes
		order.address = address
		order.name = name
		order.phoneNumber = phoneNumber
		order.costDishes = costDishes
		order.costShip = costShip

		const newOrder = await this.orderRepository.save(order)
		const orderResponse = await this.orderRepository.aggregate([
			{
			  $match: {
				  _id: newOrder._id
			  }
			},
			{
			  $lookup: {
				from: "user",
				localField: "userId",
				foreignField: "_id",
				as: "user"
			  }
			},
			{
				$lookup: {
				  from: "shop",
				  localField: "shopId",
				  foreignField: "_id",
				  as: "shop"
				}
			  },
			{
			  $unwind: "$user"
			}
		  ]).toArray();
		return orderResponse[0]
	}

	async changeStatus (_id: string, status: string): Promise<String>{
		try {
			const order = await this.orderRepository.findOne({ _id })
			if (!order){
				return '409'
			}
			await this.orderRepository.save({ ...order, status })
			return '200'
		}catch(error){
			throw new ApolloError(
                error,
                error.extensions ? error.extensions.code : '403'
              )
		}
	}

	// Shipper nhận đơn hàng mới
	async shipperBookOrder (currentShipperId: string, _id: string): Promise<String>{
		console.log(currentShipperId, _id)
		try {
			const order = await this.orderRepository.findOne({ _id })
			console.log(order)
			if (!order){
				return '409'
			}
			await this.orderRepository.save({ ...order, status: 'shipperReceiveOrder', shipperId: currentShipperId })
			return '200'
		}catch(error){
			throw new ApolloError(
                error,
                error.extensions ? error.extensions.code : '403'
              )
		}
	}

	// Đơn hàng shipper đã nhận
	async orderShipperBooked(currentShipperId: string): Promise<OrderResponse>{
		try {
			const orderShipperBooked = await this.orderRepository.aggregate([
				{
					$match: {
						shipperId: currentShipperId,
						$or: [{ status: 'shipperReceiveOrder' }, { status: 'shipperReceiveFood' }]
					}
				},
				{
					$lookup: {
					  from: "user",
					  localField: "userId",
					  foreignField: "_id",
					  as: "user"
					}
				  },
				  {
					  $unwind:"$user"
				  },
				  {
					  $lookup: {
						from: "shop",
						localField: "shopId",
						foreignField: "_id",
						as: "shop"
					  }
				  },
				  {
					  $unwind:"$shop"
				  },
			]).toArray()
			console.log(orderShipperBooked)
			return orderShipperBooked[0]
		}catch(error){
			throw new ApolloError(
                error,
                error.extensions ? error.extensions.code : '403'
              )
		}
	}

		// Đơn hàng shipper đã nhận
		async shipperHistoryOrders(currentShipperId: string): Promise<OrderResponse[]>{
			try {
				const orderShipperBooked = await this.orderRepository.aggregate([
					{
						$match: {
							shipperId: currentShipperId,
							$or: [{status: 'done'}, {status: 'cancel'}],
						}
					},
					{
						$lookup: {
						  from: "user",
						  localField: "userId",
						  foreignField: "_id",
						  as: "user"
						}
					  },
					  {
						  $unwind:"$user"
					  },
					  {
						  $lookup: {
							from: "shop",
							localField: "shopId",
							foreignField: "_id",
							as: "shop"
						  }
					  },
					  {
						  $unwind:"$shop"
					  },
				]).toArray()
				console.log(orderShipperBooked)
				return orderShipperBooked
			}catch(error){
				throw new ApolloError(
					error,
					error.extensions ? error.extensions.code : '403'
				  )
			}
		}

	async shopReceived (): Promise<OrderResponse[]> {
		const orderResponse = await this.orderRepository.aggregate([
			{
				$match: {
					status: 'shopReceiveOrder' 
				}
			},
			{
			  $lookup: {
				from: "user",
				localField: "userId",
				foreignField: "_id",
				as: "user"
			  }
			},
			{
				$unwind:"$user"
			},
			{
				$lookup: {
				  from: "shop",
				  localField: "shopId",
				  foreignField: "_id",
				  as: "shop"
				}
			},
			{
				$unwind:"$shop"
			},
		  ]).toArray();
		return orderResponse
	}

	async orderByUserId (userId: string): Promise<OrderResponse[]> {
		const orderResponse = await this.orderRepository.aggregate([
			{
				$match: {
					userId
				}
			},
			{
			  $lookup: {
				from: "user",
				localField: "userId",
				foreignField: "_id",
				as: "user"
			  }
			},
			{
				$unwind:"$user"
			},
			{
				$lookup: {
				  from: "shop",
				  localField: "shopId",
				  foreignField: "_id",
				  as: "shop"
				}
			},
			{
				$unwind:"$shop"
			},
			{
				$sort: {createdAt: -1}
			}
		  ]).toArray();
		return orderResponse
	}
}