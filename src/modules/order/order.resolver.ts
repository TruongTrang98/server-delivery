import {
	Resolver,
	Query,
	Mutation,
	Args,
	Subscription,
	Context
} from '@nestjs/graphql'
import { OrderService } from './order.service'
import { Order, OrderInput, OrderResponse } from './order.entity'

@Resolver('Order')
export class OrderResolver {
	constructor(
		private readonly orderService: OrderService
	) { }

	/**
	 * 
	 * Statistics
	 *
	 */

	@Query(() => [OrderResponse])
	async ordersForShopStatistics(@Context('currentUser') currentUser) {
		console.log(currentUser)
		return await this.orderService.findAllPendingStatus(currentUser)
	}

	@Query(() => [OrderResponse])
	async ordersPending(@Context('currentUser') currentUser) {
		return await this.orderService.findAllPendingStatus(currentUser)
	}

	@Query(() => [OrderResponse])
	async ordersShopDone(@Context('currentUser') currentUser, @Args('idSearch')idSearch: string) {
		return await this.orderService.findAllShopDoneStatus(currentUser, idSearch)
	}

	@Query(() => [OrderResponse])
	async ordersReceivedAndShipperReceived(@Context('currentUser') currentUser, @Args('idSearch')idSearch: string) {
		return await this.orderService.findAllReceivedAndShipperReceivedStatus(currentUser, idSearch)
	}
	@Query(() => [OrderResponse])
	async ordersShopReceived() {
		return this.orderService.shopReceived()
	}

	@Query(() => [OrderResponse])
	async orderByUser(@Context('currentUser') currentUser) {
		return this.orderService.orderByUserId(currentUser._id)
	}

	// Shipper nhận đơn hàng mới
	@Mutation(() => String)
	async shipperBookOrder(@Args('_id')_id: string, @Context('currentUser') currentUser, @Context('pubSub') pubSub) {
		const shipperReceiveOrder = await this.orderService.shipperBookOrder(currentUser._id, _id)
		const ordersShopPublish = await this.orderService.shopReceived()
		pubSub.publish('ordersShopPublish', { ordersShopPublish: ordersShopPublish })
		return shipperReceiveOrder
	}

	//Đon hàng shipper đã nhận
	@Query(() => OrderResponse)
	async orderShipperBooked(@Context('currentUser') currentUser) {
		console.log(currentUser)
		return await this.orderService.orderShipperBooked(currentUser._id)
	}

	//Đon hàng shipper đã nhận
	@Query(() => OrderResponse)
	async shipperHistoryOrders(@Context('currentUser') currentUser) {
		return await this.orderService.shipperHistoryOrders(currentUser._id)
	}

	@Mutation(() => String)
	async book(@Args('input') input: OrderInput, @Context('pubSub') pubSub) {
		const orderCreated = await this.orderService.book(input)
		const ordersCreated = await this.orderService.findAllPendingStatus({_id: input.shopId})
		pubSub.publish('ordersCreated', { ordersCreated: ordersCreated })
		return orderCreated
	}

	@Mutation(() => String)
	async changeStatus(@Args('_id') _id: string, @Args('status') status: string, @Context('pubSub') pubSub, @Context('currentUser') currentUser) {
		console.log(_id, status)
		const res = await this.orderService.changeStatus(_id, status)
		if(status === 'shopReceiveOrder') {
			const ordersShopPublish = await this.orderService.shopReceived()
			pubSub.publish('ordersShopPublish', { ordersShopPublish: ordersShopPublish })
			const ordersCreated = await this.orderService.findAllPendingStatus(currentUser)
			pubSub.publish('ordersCreated', { ordersCreated: ordersCreated })
		} else if(status === 'cancel') {
			const ordersCreated = await this.orderService.findAllPendingStatus(currentUser)
			pubSub.publish('ordersCreated', { ordersCreated: ordersCreated })
		}
		return res
	}

	@Subscription()
	async ordersCreated(@Context('pubSub') pubSub: any) {
		return await pubSub.asyncIterator('ordersCreated')
	}

	@Subscription()
	async ordersShopPublish(@Context('pubSub') pubSub: any) {
		return await pubSub.asyncIterator('ordersShopPublish')
	}
}