import {
	Entity,
	ObjectIdColumn,
	Column,
	CreateDateColumn,
	UpdateDateColumn,
	BeforeInsert,
} from 'typeorm';
import * as uuid from 'uuid'
import { User } from '../user/user.entity';
import { Shop } from '../shop/shop.entity';

export class DishInfo {
    _id: string
    name: string
    count: number
    price: string
}

@Entity()
export class Order {
    @ObjectIdColumn()
    _id: string

    @Column()
    name: String

    @Column()
    address: string

    @Column()
    phoneNumber: string

    @Column()
    shopId: String

    @Column()
    userId: string

    @Column()
    shipperId: string

    @Column()
    dishes: DishInfo[]

    @Column()
    costDishes: string

    @Column()
    costShip: string

    @Column()
    status: string

    @Column()
    reason: string

    @CreateDateColumn()
    createdAt: string

    @UpdateDateColumn()
    updatedAt: string

    @BeforeInsert()
	async b4register() {
    this._id = await uuid.v4()
    this.status = 'pending'
	}
}

export class OrderResponse {
    _id: string
    name: string
    shopId: string
    user: User
    phoneNumber: string
    shipperId: string
    dishes: DishInfo[]
    address: string
    status: string
    reason: string
    costDishes: string
    costShip: string
    createdAt: string
    updatedAt: string
  }

export class OrderInput {
    name: string
    address: string
    phoneNumber: string
    shopId: string
    userId: string
    dishes: DishInfo[]
    costDishes: string
    costShip: string
  }