import { Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { MongoRepository } from 'typeorm'
import * as jwt from 'jsonwebtoken'
import { ApolloError } from 'apollo-server-core'
import { Role } from './role.entity'
import * as uuid from 'uuid'

@Injectable()
export class RoleService {
	constructor(
		@InjectRepository(Role)
		private readonly roleRepository: MongoRepository<Role>
    ) {}

    async findById (_id: string): Promise<Role> {
        return await this.roleRepository.findOne({_id})
    }

    async findByCode (code: string): Promise<Role> {
        return await this.roleRepository.findOne({code})
    }

    async create (code: string): Promise<Role> {
        return await this.roleRepository.save({_id: uuid.v4(), code})
    }

}