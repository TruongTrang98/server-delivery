import {
	Entity,
	ObjectIdColumn,
	Column,
} from 'typeorm'

@Entity()
export class Role {
    @ObjectIdColumn()
    _id: string

    @Column()
    code: string
}