import {
	Resolver,
	Query,
	Mutation,
	Args,
	Subscription,
	Context
} from '@nestjs/graphql'
import { RoleService } from './role.service'
import {
    Role
} from './role.entity'

@Resolver('Role')
export class RoleResolver {
	constructor(
		private readonly roleService: RoleService
    ) {}

    @Mutation(() => Role)
    async create (@Args('code')code: string){
        return await this.roleService.create(code)
    }
}
