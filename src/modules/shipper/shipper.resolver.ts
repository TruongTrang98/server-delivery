import { ShipperService } from './shipper.service';
import {
	Resolver,
	Query,
	Mutation,
    Args,
    Context
} from '@nestjs/graphql'
import { ShipperResponse, ShipperInput } from './shipper.entity';
import { ImageInput } from '../image/image.entity';

@Resolver('Shipper')
export class ShipperResolver {
    constructor(private readonly shipperService: ShipperService) { }

    @Query(() => ShipperResponse)
    async meShipper(@Context('currentUser')currentUser) {
        console.log(currentUser)
        return await this.shipperService.findById(currentUser._id);
    }

    @Query(() => [ShipperResponse])
    async shippers(
        @Args('txtSearch') txtSearch: string, 
		@Args('txtFilter') txtFilter: string, 
		@Args('offset') offset: number, 
		@Args('limit') limit: number
    ) {
        return await this.shipperService.findAll({ txtSearch, txtFilter, offset, limit })
    }

    @Query(() => Number)
	async numberOfShipper(@Args('txtSearch') txtSearch: string, @Args('txtFilter') txtFilter: string) {
		return await this.shipperService.numberOfShipper(txtSearch, txtFilter)
	}

    @Query(() => ShipperResponse)
    async shipperById(@Args('_id')_id: string) {
        return await this.shipperService.findById(_id)
    }

    @Mutation(() => String)
    async createShipper(@Args('input')input: ShipperInput){
        return await this.shipperService.create(input)
    }

    @Mutation(() => String)
    async updateShipperInfo(@Args('_id')_id: string, @Args('input')input: ShipperInput){
        return await this.shipperService.updateInfo(_id, input);
    }

    @Mutation(() => String)
	async updateShipperAvatar(
		@Args('_id') _id: string,
		@Args('image') image: ImageInput
	) {
		return await this.shipperService.updateAvatar(_id, image)
	}
}
