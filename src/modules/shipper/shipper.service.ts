import { Injectable } from '@nestjs/common';
import { Shipper, ShipperInput, ShipperResponse } from './shipper.entity';
import { MongoRepository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { ApolloError } from 'apollo-server-core';
import { AccountService } from '../account/account.service';
import * as fs from 'fs'
import { ImageService } from '../image/image.service';
import { ImageInput } from '../image/image.entity';

@Injectable()
export class ShipperService {
  constructor(
    @InjectRepository(Shipper)
    private readonly shipperRepository: MongoRepository<Shipper>,
    private readonly accountService: AccountService,
    private readonly imageService: ImageService
  ) { }

  async findById(_id: string): Promise<ShipperResponse> {
    const shipperResponse = await this.shipperRepository.aggregate([
      {
        $match: {
          _id
        }
      },
      {
        $lookup: {
          from: "account",
          localField: "accountId",
          foreignField: "_id",
          as: "account"
        }
      },
      {
        $unwind: "$account"
      }
    ]).toArray();
    return shipperResponse[0]
  }


  async findAll({ txtSearch, txtFilter, offset, limit }): Promise<ShipperResponse[]> {
    const shipperResponse = await this.shipperRepository.aggregate([
      {
        $lookup: {
          from: "account",
          localField: "accountId",
          foreignField: "_id",
          as: "account"
        }
      },
      {
        $unwind: "$account"
      },
      {
				$match: {
					name: { $regex: !!txtSearch ? `${txtSearch}` : `` },
					$or: (txtFilter === 'all' || !txtFilter) ? [{ "account.isLocked": false }, { "account.isLocked": true }] 
						: txtFilter === 'isLocked' ? [{ "account.isLocked": true }]
						: [{ "account.isLocked": false }]
				}
			},
			{
                $skip: +offset
            },
            {
                $limit: +limit
			}
    ]).toArray()

    return shipperResponse
  }

  async numberOfShipper (txtSearch: string, txtFilter: string): Promise<number> {
		return (await this.shipperRepository.aggregate([
			{
				$lookup: {
					from: "account",
					localField: "accountId",
					foreignField: "_id",
					as: "account"
				}
			},
			{
				$unwind: "$account"
			},
			{
				$match: {
					name: { $regex: !!txtSearch ? `${txtSearch}` : `` },
					$or: (txtFilter === 'all' || !txtFilter) ? [{ "account.isLocked": false }, { "account.isLocked": true }] 
						: txtFilter === 'isLocked' ? [{ "account.isLocked": true }]
						: [{ "account.isLocked": false }]
				}
			}
		]).toArray()).length
	}

  async create(input: ShipperInput): Promise<String> {
    try {
      const { name, address, image, gender, email, phoneNumber, cmnd, password } = input
      const existedShipperEmail = await this.shipperRepository.findOne({ email })
      const existedShopPhone = await this.shipperRepository.findOne({ phoneNumber })

      if (existedShipperEmail) {
        return '410'
      }
      if (existedShopPhone) {
        return '411'
      }

      const account = await this.accountService.create({ username: email, password, role: "shipper" })
      if (account) {
        const shipper = new Shipper()
        shipper.name = name
        shipper.address = address
        shipper.imageUrl = `${account._id}.${image.type.split('/')[1]}`
        shipper.gender = gender
        shipper.email = email
        shipper.phoneNumber = phoneNumber
        shipper.cmnd = cmnd
        shipper.accountId = account._id
        await this.shipperRepository.save(shipper)
        await this.imageService.updateImage(account._id, image)
        return '200'
      }
    } catch (error) {
      throw new ApolloError(
        error,
        error.extensions ? error.extensions.code : '403'
      )
    }
  }

  async updateInfo(_id: string, input: ShipperInput): Promise<String> {
    try {
      //const { name, phoneNumber, email, address, password } = input
      delete input.image
      delete input.password
      const existedShipper = await this.shipperRepository.findOne({ _id })
      if (!existedShipper) {
        return '409'
      }
      await this.shipperRepository.save({ _id, ...input, imageUrl: existedShipper.imageUrl })
      return '200'

    } catch (error) {
      throw new ApolloError(
        error,
        error.extensions ? error.extensions.code : '403'
      )
    }
  }

  async updateAvatar(_id: string, image: ImageInput): Promise<String> {
		try {
			let shipper = await this.shipperRepository.findOne({ _id })
			if(!shipper) {
				return '409'
			}
			shipper.imageUrl = `${_id}.${image.type.split('/')[1]}`
			await this.shipperRepository.save(shipper)
			await this.imageService.updateImage(_id, image)
			return '200'
		} catch (error) {
			throw new ApolloError(
				error,
				error.extensions ? error.extensions.code : '403'
			)
		}
	}

}
