import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Shipper } from './shipper.entity';
import { ShipperResolver } from './shipper.resolver';
import { ShipperService } from './shipper.service';
import { AccountModule } from '../account/account.module';
import { ImageModule } from '../image/image.module';

@Module({
    imports: [TypeOrmModule.forFeature([Shipper]), AccountModule, ImageModule],
	providers: [ShipperResolver, ShipperService],
    exports: [ShipperService]
})
export class ShipperModule {}
