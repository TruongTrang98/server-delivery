import {
	Entity,
	ObjectIdColumn,
	Column,
	CreateDateColumn,
	UpdateDateColumn,
	BeforeInsert,
	BeforeUpdate
} from 'typeorm';
import * as uuid from 'uuid'
import { Account } from '../account/account.entity';
import { ImageInput } from '../image/image.entity';

export class ShipperInput{
	name: string
	address: string
	image: ImageInput
	gender: string
	email: string
	phoneNumber: string
	cmnd: string
	password: string

}

export class ShipperResponse {
	_id: string
	name: string
	address: string
	imageUrl: string
	gender: string
	email: string
	phoneNumber: string
	cmnd: string
	account: Account
	isActive: boolean
	createdAt: string
	updatedAt: string
}

@Entity({})
export class Shipper{
    @ObjectIdColumn()
    _id: string

    @Column()
    name: string
    
	@Column()
	address: string
	
	@Column()
	imageUrl: string

	@Column()
	gender: string

    @Column()
    email:string

    @Column()
    phoneNumber: string

    @Column()
	cmnd: string
	
	@Column()
	accountId: string

    @Column()
    isActive: boolean

    @CreateDateColumn()
    createdAt: string

    @UpdateDateColumn()
    updatedAt: string

    @BeforeInsert()
	async b4register() {
		const uid = await uuid.v4()
		this._id = uid
		this.isActive = true
	}
}