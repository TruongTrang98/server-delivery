import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Account } from './account.entity';
import { AccountResolver } from './account.resolver';
import { AccountService } from './account.service';
import { RoleModule } from '../role/role.module';

@Module({
    imports: [TypeOrmModule.forFeature([Account]), RoleModule],
	providers: [AccountResolver, AccountService],
    exports: [AccountService]
})
export class AccountModule{}
