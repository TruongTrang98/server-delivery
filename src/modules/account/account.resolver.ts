import {
	Resolver,
	Query,
	Mutation,
	Args,
	Subscription,
	Context
} from '@nestjs/graphql'
import { AccountService } from './account.service'
import {
	LoginResponse,
	LoginInput,
    Account,
	ChangePasswordInput,
	AccountInput
} from './account.entity'

@Resolver('Account')
export class AccountResolver {
	constructor(
		private readonly accountService: AccountService
	) {}

	// @Mutation(() => String)
	// async createAccount(@Args('input')input: CreateAccountInput){
	// 	return await this.accountService.create(input)
	// }


	@Mutation(() => LoginResponse)
	async adminLogin(@Args('input') input: LoginInput) {
		return await this.accountService.adminLogin(input)
	}

	@Mutation(() => LoginResponse)
	async shopLogin(@Args('input') input: LoginInput) {
		return await this.accountService.shopLogin(input)
	}

	@Mutation(() => LoginResponse)
	async userLogin(@Args('input') input: LoginInput) {
		return await this.accountService.userLogin(input)
	}

	@Mutation(() => LoginResponse)
	async shipperLogin(@Args('input') input: LoginInput) {
		console.log(input)
		return await this.accountService.shipperLogin(input)
	}

	@Mutation(() => LoginResponse)
	async lockAndUnlockAccount(@Args('_id') _id: string, @Args('reason') reason: string) {
		return await this.accountService.lockAndUnlock(_id, reason)
	}

	@Mutation(() => String)
	async changePassword(@Context('currentUser')currentUser: string, @Args('input')input: ChangePasswordInput){
		return await this.accountService.changePassword(currentUser, input);
	}

	@Mutation(() => Account)
	async createAccount (@Args('input') input: AccountInput) {
		return this.accountService.create(input)
	}

	// @Subscription()
	// async userCreated(@Context('pubSub') pubSub: any) {
	// 	return await pubSub.asyncIterator('userCreated')
	// }
}
