import {
	Entity,
	ObjectIdColumn,
	Column,
	CreateDateColumn,
	UpdateDateColumn,
	BeforeInsert,
	BeforeUpdate
} from 'typeorm'
import * as uuid from 'uuid'
import * as bcrypt from 'bcrypt'
import {
	IsString,
	IsNotEmpty,
	Length,
	MinLength,
	IsBoolean
} from 'class-validator'

export class LoginInput {
	@MinLength(4, {
		message: 'Your username must be at least 4 characters'
	})
	@IsString()
	@IsNotEmpty()
	username: string

	@Length(1, 8, {
		message: 'Your password must be between 1 and 8 characters'
	})
	@IsString()
	@IsNotEmpty()
	password: string

}

export class AccountInput {
	@MinLength(4, {
		message: 'Your username must be at least 4 characters'
	})
	@IsString()
	@IsNotEmpty({ message: 'Your username can not be blank' })
	username: string

	@Length(1, 8, {
		message: 'Your password must be between 1 and 8 characters'
	})
	@IsString()
	@IsNotEmpty({ message: 'Your password can not be blank' })
	password: string

	@IsString()
	@IsNotEmpty()
	role: string
}

export class LoginResponse {
	@IsString()
	@IsNotEmpty()
    token: string
    profile: string
}

export class ChangePasswordInput{
	oldPassword: string
	newPassword: string
}

@Entity()
export class Account {
	@ObjectIdColumn()
	_id: string

	@Column()
	@IsString()
	@IsNotEmpty()
	username: string

	@Column()
	@IsString()
	@IsNotEmpty()
	password: string

	@Column()
	@IsString()
	@IsNotEmpty()
	roleId: string

	@Column()
	@IsBoolean()
	@IsNotEmpty()
	isLocked: boolean

	@Column()
	@IsString()
	reason: string

	@Column()
	@IsBoolean()
	@IsNotEmpty()
	isActive: boolean

	@CreateDateColumn()
	createdAt: string
	@UpdateDateColumn()
	updatedAt: string

	@BeforeInsert()
	async b4register() {
		this._id = await uuid.v4()
		this.password = await bcrypt.hash(this.password, 10)
		this.isLocked = false
		this.reason = ''
		this.isActive = true
	}

	@BeforeUpdate()
	async b4update() {
		this.password = await bcrypt.hash(this.password, 10)
	}

	async matchesPassword(password) {
		return await bcrypt.compare(password, this.password)
	}
}
