import { Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { MongoRepository } from 'typeorm'
import * as jwt from 'jsonwebtoken'
import { ApolloError } from 'apollo-server-core'
import {
	LoginResponse,
	LoginInput,
	Account,
	ChangePasswordInput,
	AccountInput
} from './account.entity'
import { RoleService } from '../role/role.service'
import { Context } from '@nestjs/graphql'
import * as bcrypt from 'bcrypt'

@Injectable()
export class AccountService {
	constructor(
		@InjectRepository(Account)
		private readonly accountRepository: MongoRepository<Account>,
		private readonly roleService: RoleService
	) { }

	async findById(_id: string): Promise<Account> {
		try {
			const message = 'Not Found: Account'
			const code = '404'
			const additionalProperties = {}

			const account = await this.accountRepository.findOne({ _id })

			if (!account) {
				throw new ApolloError(message, code, additionalProperties)
			}

			return account
		} catch (error) {
			throw new ApolloError(error, '500', {})
		}
	}

	async create(input: AccountInput): Promise<Account> {
		const message = 'Conflict: Username'
		const code = '409'
		const additionalProperties = {}

		const { username, password, role } = input

		const existedUser = await this.accountRepository.findOne({ username })
		const existedRole = await this.roleService.findByCode(role)

		if (existedUser) {
			throw new ApolloError(message, code, additionalProperties)
		}

		const account = new Account()
		account.username = username
		account.password = password
		account.roleId = existedRole._id
		return await this.accountRepository.save(account)
	}

	async adminLogin(input: LoginInput): Promise<LoginResponse> {
		const message = 'Unauthorized'
		const code = '401'
		const additionalProperties = {}

		const { username, password } = input

		const account = await this.accountRepository.findOne({ username })
		const role = await this.roleService.findById(account.roleId)

		if (!account || !(await account.matchesPassword(password)) || role.code !== 'admin') {
			throw new ApolloError(message, code, additionalProperties)
		}

		const activeMessage = 'Gone'
		const activeCode = '404'
		const activeAdditionalProperties = {}

		if (!account.isActive) {
			throw new ApolloError(
				activeMessage,
				activeCode,
				activeAdditionalProperties
			)
		}

		const lockedMessage = 'Locked'
		const lockedCode = '423'
		const lockedAdditionalProperties = {}

		if (account.isLocked) {
			throw new ApolloError(
				lockedMessage,
				lockedCode,
				lockedAdditionalProperties
			)
		}

		const token = jwt.sign(
			{
				_id: account._id,
				audience: account.username,
				role: role.code
			},
			process.env.SECRET_KEY,
			{
				expiresIn: '30d'
			}
		)

		return { token, profile: role.code }
	}


	async shopLogin(input: LoginInput): Promise<LoginResponse> {
		const message = 'Unauthorized'
		const code = '401'
		const additionalProperties = {}

		const { username, password } = input

		const account = await this.accountRepository.findOne({ username })
		const role = await this.roleService.findById(account.roleId)

		if (!account || !(await account.matchesPassword(password)) || role.code !== 'partner') {
			throw new ApolloError(message, code, additionalProperties)
		}

		const activeMessage = 'Gone'
		const activeCode = '404'
		const activeAdditionalProperties = {}

		if (!account.isActive) {
			throw new ApolloError(
				activeMessage,
				activeCode,
				activeAdditionalProperties
			)
		}

		const lockedMessage = 'Locked'
		const lockedCode = '423'
		const lockedAdditionalProperties = {}

		if (account.isLocked) {
			throw new ApolloError(
				lockedMessage,
				lockedCode,
				lockedAdditionalProperties
			)
		}

		const token = jwt.sign(
			{
				_id: account._id,
				audience: account.username,
				role: role.code
			},
			process.env.SECRET_KEY,
			{
				expiresIn: '30d'
			}
		)
		console.log(token);

		return { token, profile: role.code }
	}

	async userLogin(input: LoginInput): Promise<LoginResponse> {
		const message = 'Unauthorized'
		const code = '401'
		const additionalProperties = {}

		const { username, password } = input

		const account = await this.accountRepository.findOne({ username })
		const role = await this.roleService.findById(account.roleId)

		if (!account || !(await account.matchesPassword(password)) || role.code !== 'user') {
			throw new ApolloError(message, code, additionalProperties)
		}

		const activeMessage = 'Gone'
		const activeCode = '404'
		const activeAdditionalProperties = {}

		if (!account.isActive) {
			throw new ApolloError(
				activeMessage,
				activeCode,
				activeAdditionalProperties
			)
		}

		const lockedMessage = 'Locked'
		const lockedCode = '423'
		const lockedAdditionalProperties = {}

		if (account.isLocked) {
			throw new ApolloError(
				lockedMessage,
				lockedCode,
				lockedAdditionalProperties
			)
		}

		const token = jwt.sign(
			{
				_id: account._id,
				audience: account.username,
				role: role.code
			},
			process.env.SECRET_KEY,
			{
				expiresIn: '30d'
			}
		)
		return { token, profile: role.code }
	}

	async shipperLogin(input: LoginInput): Promise<LoginResponse> {
		const message = 'Unauthorized'
		const code = '401'
		const additionalProperties = {}

		const { username, password } = input

		const account = await this.accountRepository.findOne({ username })
		const role = await this.roleService.findById(account.roleId)

		if (!account || !(await account.matchesPassword(password)) || role.code !== 'shipper') {
			throw new ApolloError(message, code, additionalProperties)
		}

		const activeMessage = 'Gone'
		const activeCode = '404'
		const activeAdditionalProperties = {}

		if (!account.isActive) {
			throw new ApolloError(
				activeMessage,
				activeCode,
				activeAdditionalProperties
			)
		}

		const lockedMessage = 'Locked'
		const lockedCode = '423'
		const lockedAdditionalProperties = {}

		if (account.isLocked) {
			throw new ApolloError(
				lockedMessage,
				lockedCode,
				lockedAdditionalProperties
			)
		}

		const token = jwt.sign(
			{
				_id: account._id,
				audience: account.username,
				role: role.code
			},
			process.env.SECRET_KEY,
			{
				expiresIn: '30d'
			}
		)
		return { token, profile: role.code }
	}


	async lockAndUnlock(_id: string, reason: string): Promise<String> {
		try {
			const existAccount = await this.accountRepository.findOne({ _id })
			if (existAccount) {
				await this.accountRepository.save({ ...existAccount, isLocked: !existAccount.isLocked, reason })
				return '200'
			} else {
				return '409'
			}
		} catch (error) {
			throw new ApolloError(
				error,
				error.extensions ? error.extensions.code : '403'
			)
		}
	}

	async changePassword(currentUser, input: ChangePasswordInput): Promise<String> {
		try {
			const {oldPassword, newPassword} = input;
			// console.log(currentId, oldPassword, newPassword);
			const existedAccount = await this.accountRepository.find({_id: currentUser.accountId})
			// console.log(existedAccount[0].password)
			const check = await bcrypt.compare(oldPassword, existedAccount[0].password)
			if(!check){
				return '409'
			}
			const newPass = await bcrypt.hash(newPassword, 10);
			// console.log(newPass);
			await this.accountRepository.save({ _id: currentUser.accountId, password: newPass })
			
			return '200';
		} catch (error) {
			throw new ApolloError(
				error,
				error.extensions ? error.extensions.code : '403'
			)
		}
	}
}
