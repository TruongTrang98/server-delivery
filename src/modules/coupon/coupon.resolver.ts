import { 
    Resolver,
    Query,
    Mutation,
    Args,
    Context
} from '@nestjs/graphql';
import { CouponService } from './coupon.service';
import { CouponResponse, CouponInput } from './coupon.entity';

@Resolver('Coupon')
export class CouponResolver {
    constructor(
        private readonly couponService: CouponService
    ) {}

    @Query(() => CouponResponse)
    async coupon(@Context('currentUser')currentUser, @Args('_id')_id: string) {
        return await this.couponService.couponById(currentUser, _id);
    }

    @Query(() => [CouponResponse])
    async coupons(@Context('currentUser')currentUser) {
        const res = await this.couponService.coupons(currentUser._id);
        return res;
    }

    @Query(() => [CouponResponse])
    // List shop have coupon display at the homepage
    async couponsForUser() {
        return this.couponService.couponsForUser();
    }

    @Mutation(() => String)
    async createCoupon(@Context('currentUser')currentUser, @Args('input')input: CouponInput){
        return await this.couponService.createCoupon(currentUser._id, input)
    }

    @Mutation(() => String)
    async updateCoupon(@Context('currentUser')currentUser, @Args('_id')_id: string, @Args('input')input: CouponInput){
        return await this.couponService.updateCoupon(currentUser._id, _id, input)
    }

    @Mutation(() => String)
    async deleteCoupon(@Context('currentUser')currentUser, @Args('_id')_id: string){
        return await this.couponService.deleteCoupon(currentUser._id, _id);
    }

    @Mutation(() => String)
    async deleteDishInCoupon(@Context('currentUser')currentUser, @Args('couponId')couponId: string, @Args('dishId')dishId: string){
        return await this.couponService.deleteDishInCoupon(currentUser._id, couponId, dishId)
    }
}
