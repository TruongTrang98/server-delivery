import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CouponResolver } from './coupon.resolver';
import { CouponService } from './coupon.service';
import { Coupon } from './coupon.entity';
import { DishModule } from '../dish/dish.module';

@Module({
    imports: [TypeOrmModule.forFeature([Coupon]), DishModule],
    providers: [CouponResolver, CouponService],
    exports: [CouponService]
})
export class CouponModule {}
