import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Coupon, CouponInput, CouponResponse } from './coupon.entity';
import { MongoRepository } from 'typeorm';
import { ApolloError } from 'apollo-server-core';
import { DishService } from '../dish/dish.service';

@Injectable()
export class CouponService {
    constructor(
        @InjectRepository(Coupon)
        private readonly couponRespository: MongoRepository<Coupon>,
        private readonly dishRespository: DishService
    ) {}

    async couponById(currentUser: any, _id: string): Promise<CouponResponse> {
        const couponResponse = await this.couponRespository.aggregate([
            {
                $match: {
                    _id
                }
            },
            {
                $lookup: {
                    from: 'dish',
                    localField: 'dishes',
                    foreignField: '_id',
                    as: 'dishes'
                }
            },
        ]).toArray()
        const res = await this.dishRespository.dishesWithoutCoupon(currentUser);
        couponResponse[0].dishes.push(...res)
        console.log(couponResponse[0]);

        return couponResponse[0]
    }

    async coupons(shopId: string): Promise<CouponResponse[]> {
        const couponResponse = await this.couponRespository.aggregate([
            {
                $match: {
                    shopId
                }
            },
            {
                $lookup: {
                    from: "dish",
                    localField: "dishes",
                    foreignField: "_id",
                    as: "dishesObject"
                }
            },
            {
                $unwind: {
                    path: "$dishesObject",
                    "preserveNullAndEmptyArrays": true
                },
            },
            {
                $group: {
                    _id: "$_id",
                    content: {
                        $first: "$content"
                    },
                    percent: {
                        $first: "$percent"
                    },
                    shopId: {
                        $first: "$shopId"
                    },
                    dishes: { $push: "$dishesObject" },
                    createdAt: {
                        $first: "$createdAt"
                    },
                    updatedAt: {
                        $first: "$updatedAt"
                    }
                }
            },
        ]).toArray()
        return couponResponse
    }

    async couponsForUser(): Promise<CouponResponse[]> {
        const couponResponse = await this.couponRespository.aggregate([
            {
                $lookup: {
                    from: "shop",
                    localField: "shopId",
                    foreignField: "_id",
                    as: "shop"
                }               
            },
            {
                $unwind : "$shop"
            },
            {
                $lookup: {
                    from: "account",
                    localField: "shop.accountId",
                    foreignField: "_id",
                    as: "account"
                }
            },
            {
                $unwind : "$account"
            },
            { $sort : { updatedAt : -1 } },
            {
                $match: {
                    "account.isLocked": false
                }
            },
            {
                $limit: 10
            }
        ]).toArray()
        console.log(couponResponse)
        return couponResponse
    }
 
    async createCoupon(shopId: string, input: CouponInput): Promise<String> {
        try {
            const newCoupon = await this.couponRespository.create({ shopId, ...input })
            const dishesInCoupon = input.dishes
            if(newCoupon) {
                const res = await this.couponRespository.save(newCoupon);
                /**
                 * Có id của coupon vừa tạo là res._id, update vào dish
                 * Có update dish chỗ này ko
                 */
                for(let i=0;i<dishesInCoupon.length;i++){
                    const res1 = await this.dishRespository.updateDishCoupon(shopId, dishesInCoupon[i], {name: '', imageUrl:'', price: null, couponId: res._id})
                    console.log('RES1', res1);
                }
                return '200'
            } else {
                return '408'
            }

        } catch (error) {
            throw new ApolloError(
                error,
                error.extensions ? error.extensions.code : '403'
            )
        }
    }

    async updateCoupon(shopId: string, _id: string, input: CouponInput) {
        try {
            // const shop = await this.shopService.findByAccountId(accountId)
            // const shopId = shop._id
            const existedCoupon = await this.couponRespository.findOne({ _id })
            if (existedCoupon.shopId != shopId) return '409'
            if (!existedCoupon) {
                return '409'
            }
            const oldDishes = existedCoupon.dishes
            const newDishes = input.dishes
            const deletedDishes = oldDishes.filter(item => !newDishes.includes(item));
            deletedDishes.map(item => {
                this.dishRespository.deleteCouponIdInDish(shopId, item)
            })
            const addNewDishes = newDishes.filter(item => !oldDishes.includes(item));
            addNewDishes.map(item => {
                this.dishRespository.addCouponIdToDish(shopId, item, existedCoupon._id)
            })
            // const newDishesAfterUpdate = oldDishes.filter(item => !deletedDishes.includes(item));
            // const finalDishes = [...newDishesAfterUpdate, ...addNewDishes];
            
            // console.log(oldDishes, newDishes, deletedDishes, addNewDishes, finalDishes)
            //Delete couponId in dishes be deleted in coupon
            // deletedDishes.map(item => {
            //     await this.dishRespository.updateDish(item, )
            // })
             
            await this.couponRespository.save({ _id, ...input })
            return '200'
        } catch (error) {
            throw new ApolloError(
                error,
                error.extensions ? error.extensions.code : '403'
            )
        }
    }
    

    async deleteCoupon (shopId: string, _id: string): Promise<String> {
        try {
            // const shop = await this.shopService.findByAccountId(accountId)
            // const shopId = shop._id
            const existedCoupon = await this.couponRespository.findOne({ _id })
            if (existedCoupon.shopId != shopId) return '409'
            if (!existedCoupon) {
                return '409'
            }
            await this.couponRespository.delete({ _id });
            for(let i = 0;i<existedCoupon.dishes.length;i++){
                await this.dishRespository.updateDishCoupon(shopId, existedCoupon.dishes[i], {name: '', imageUrl:'', price: null, couponId: null})
            }
            return '200'
        } catch (error) {
            throw new ApolloError(
                error,
                error.extensions ? error.extensions.code : '403'
            )
        }
    }

    async deleteDishInCoupon(shopId: string, couponId: string, dishId: string): Promise<String> {
        try {
            // const shop = await this.shopService.findByAccountId(accountId)
            // const shopId = shop._id
            const existedCoupon = await this.couponRespository.findOne({ _id: couponId })
            if (existedCoupon.shopId != shopId) return '409'
            console.log(existedCoupon.dishes);
            if(!existedCoupon.dishes.includes(dishId)){
                return '409'
            }
            existedCoupon.dishes.splice(existedCoupon.dishes.indexOf(dishId),1)            
            // console.log(newMenu, existedCoupon.dishes);
            await this.couponRespository.update({_id: couponId}, { ...existedCoupon, dishes: existedCoupon.dishes })
            await this.dishRespository.updateDishCoupon(shopId, dishId, {name: '', imageUrl:'', price: null, couponId: null})
            return '200'
        } catch (error) {
            throw new ApolloError(
                error,
                error.extensions ? error.extensions.code : '403'
            )
        }
    }
}
