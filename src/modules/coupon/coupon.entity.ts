import {
    Entity,
    ObjectIdColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    BeforeInsert
} from 'typeorm'
import * as uuid from 'uuid'
import { Dish } from '../dish/dish.entity';
import { Shop } from '../shop/shop.entity';

@Entity()
export class Coupon {
    @ObjectIdColumn()
    _id: string

    @Column()
    content: string

    @Column()
    percent: string

    @Column()
    dishes: string[]

    @Column()
    shopId: string

    @CreateDateColumn()
    createdAt: string

    @UpdateDateColumn()
    updatedAt: string

    @BeforeInsert()
	async beforeInsert() {
		this._id = await uuid.v4()
	}
}

export class CouponInput {
    content: string
    percent: string
    dishes: [string]
}

export class CouponResponse {
    _id: string
    content: string
    percent: string
    dishes: [Dish]
    shop: Shop
    createdAt: string
    updatedAt: string
}