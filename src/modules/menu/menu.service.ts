import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Menu, MenuResponse, MenuInput } from './menu.entity';
import { MongoRepository } from 'typeorm';
import { ApolloError } from 'apollo-server-core';
import { ShopService } from '../shop/shop.service';
import { Dish } from '../dish/dish.entity';

@Injectable()
export class MenuService {
    constructor(
        @InjectRepository(Menu)
        private readonly menuRespository: MongoRepository<Menu>,
        private readonly shopService: ShopService
    ) { }

    async menuById(_id: string): Promise<MenuResponse> {
        const menuResponse = await this.menuRespository.aggregate([
            {
                $match: {
                    _id
                }
            },
            {
                $lookup: {
                    from: "shop",
                    localField: "shopId",
                    foreignField: "_id",
                    as: "shop"
                }
            },
            {
                $unwind: "$shop"
            },
            {
                $lookup: {
                    from: "dish",
                    localField: "dishes",
                    foreignField: "_id",
                    as: "dishes"
                }
            },
        ]).toArray()
        return menuResponse[0];
    }

    async menus(shopId: string): Promise<MenuResponse[]> {
        // const shop = await this.shopService.findByAccountId(accountId);
        // const shopId = shop._id;
        const menusResponse = this.menuRespository.aggregate([
            {
                $match: {
                    shopId
                }
            },
            // {
            //     $unwind: "$dishes"
            // },
            {
                $lookup: {
                    from: "dish",
                    localField: "dishes",
                    foreignField: "_id",
                    as: "dishesObject"
                }
            },
            {
                $unwind: {
                    path: "$dishesObject",
                    "preserveNullAndEmptyArrays": true
                },
            },
            {
                $lookup: {
                    from: "coupon",
                    localField: "dishesObject.couponId",
                    foreignField: "_id",
                    as: "couponObject"
                }
            },
            {
                $unwind: {
                    path: "$couponObject",
                    "preserveNullAndEmptyArrays": true
                },
            },
            // {
            //     $group: {
            //         _id: "$dishesObject._id",
            //         dishId: {
            //             $first: "$dishesObject._id"
            //         },
            //         dishName: {
            //             $first: "$dishesObject.name"
            //         },
            //         dishPrice: {
            //             $first: "$dishesObject.price"
            //         },
            //         couponObj: {
            //             $push: "$couponObject"
            //         }
            //     }
            // },
            {
                $group: {
                    _id: "$_id",
                    name: {
                        $first: "$name"
                    },
                    shopId: {
                        $first: "$shopId"
                    },
                    dishes: {
                        $push: {
                            _id: "$dishesObject._id",
                            name: "$dishesObject.name",
                            price: "$dishesObject.price",
                            imageUrl: "$dishesObject.imageUrl",
                            coupon:  "$couponObject",
                            createdAt: "$dishesObject.createdAt",
                            updatedAt: "$dishesObject.updatedAt",
                        }
                    },
                    isActive: {
                        $first: "$isActive"
                    },
                    createdAt: {
                        $first: "$createdAt"
                    },
                    updatedAt: {
                        $first: "$updatedAt"
                    }
                }
            }
        ]).toArray()
        return menusResponse;
    }

    async createMenu(shopId: string, input: MenuInput): Promise<String> {
        try {
            // const shop = await this.shopService.findByAccountId(accountId)
            // const shopId = shop._id
            // console.log(input)
            const newMenu = await this.menuRespository.create({ shopId, ...input });
            if (newMenu) {
                await this.menuRespository.save(newMenu);
                return '200'
            } else {
                return '408'
            }
        }
        catch (error) {
            throw new ApolloError(
                error,
                error.extensions ? error.extensions.code : '403'
            )
        }

    }

    async updateMenu(shopId: string, _id: string, input: MenuInput) {
        try {
            // const shop = await this.shopService.findByAccountId(accountId)
            // const shopId = shop._id
            const existedMenu = await this.menuRespository.findOne({ _id })
            if (existedMenu.shopId != shopId) return '409'
            if (!existedMenu) {
                return '409'
            }
            const oldName = existedMenu.name
            await this.menuRespository.save({ _id, ...input })
            return '200'

        } catch (error) {
            throw new ApolloError(
                error,
                error.extensions ? error.extensions.code : '403'
            )
        }
    }

    async deleteMenu(shopId: string, _id: string): Promise<String> {
        try {
            // const shop = await this.shopService.findByAccountId(accountId)
            // const shopId = shop._id
            const existedMenu = await this.menuRespository.findOne({ _id })
            if (existedMenu.shopId != shopId) return '409'
            if (!existedMenu) {
                return '409'
            }
            await this.menuRespository.delete({ _id });
            return '200'
        } catch (error) {
            throw new ApolloError(
                error,
                error.extensions ? error.extensions.code : '403'
            )
        }
    }

    async deleteDishInMenu(shopId: string, menuId: string, dishId: string): Promise<String> {
        try {
            // const shop = await this.shopService.findByAccountId(accountId)
            // const shopId = shop._id
            const existedMenu = await this.menuRespository.findOne({ _id: menuId })
            if (existedMenu.shopId != shopId) return '409'
            console.log(existedMenu.dishes);
            if (!existedMenu.dishes.includes(dishId)) {
                return '409'
            }
            existedMenu.dishes.splice(existedMenu.dishes.indexOf(dishId), 1)
            // console.log(newMenu, existedMenu.dishes);
            await this.menuRespository.update({ _id: menuId }, { ...existedMenu, dishes: existedMenu.dishes })
            return '200'
        } catch (error) {
            throw new ApolloError(
                error,
                error.extensions ? error.extensions.code : '403'
            )
        }
    }
}
