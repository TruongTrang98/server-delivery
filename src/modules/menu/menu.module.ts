import { Module } from '@nestjs/common';
import { MenuResolver } from './menu.resolver';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MenuService } from './menu.service';
import { Menu } from './menu.entity';
import { ShopModule } from '../shop/shop.module';

@Module({
  imports: [TypeOrmModule.forFeature([Menu]), ShopModule],
  providers: [MenuResolver, MenuService],
  exports: [MenuService]
})
export class MenuModule {}
