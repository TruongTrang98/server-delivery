import {
    Entity,
    Column,
    ObjectIdColumn,
    CreateDateColumn,
    UpdateDateColumn,
    BeforeInsert
} from 'typeorm';
import * as uuid from 'uuid'
import { Shop } from '../shop/shop.entity';
import { Dish, DishResponse } from '../dish/dish.entity';

@Entity() 
export class Menu {
    @ObjectIdColumn()
    _id: string

    @Column()
    name: string
    
    @Column()
    shopId: string
    
    @Column()
    dishes: string[]

    @Column()
    isActive: boolean

    @CreateDateColumn()
    createdAt: string

    @UpdateDateColumn()
    updatedAt: string

    @BeforeInsert()
	async beforeInsert() {
		this._id = await uuid.v4()
        this.isActive = true
	}
}

export class MenuInput {
    name: string
    dishes: [string]
}

export class MenuResponse {
    _id: string
    name: string
    shop: Shop
    dishes: [DishResponse]
    isActive: boolean
    createdAt: string
    updatedAt: string
}