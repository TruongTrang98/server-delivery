import {
	Resolver,
	Query,
	Mutation,
	Args,
    Context,
} from '@nestjs/graphql'
import { MenuService } from './menu.service';
import { MenuResponse, MenuInput } from './menu.entity';

@Resolver('Menu')
export class MenuResolver {
    constructor(
        private readonly menuService: MenuService
    ) {}

    @Query(() => MenuResponse)
    async menu (@Args('_id')_id: string) {
        return await this.menuService.menuById(_id);
    }

    @Query(() => [MenuResponse])
    async menus(@Context('currentUser')currentUser) {
        const res = await this.menuService.menus(currentUser._id);
        return res;
    }

    @Query(() => [MenuResponse])
    async menuByShopId(@Args('shopId')shopId: string) {
        const res = await this.menuService.menus(shopId);
        return res;
    }

    @Mutation(() => String)
    async createMenu (@Context('currentUser')currentUser, @Args('input')input: MenuInput){
        return await this.menuService.createMenu(currentUser._id, input)
    }

    @Mutation(() => String)
    async updateMenu(@Context('currentUser')currentUser, @Args('_id')_id: string, @Args('input')input: MenuInput){
        return await this.menuService.updateMenu(currentUser._id, _id, input)
    }

    @Mutation(() => String)
    async deleteMenu(@Context('currentUser')currentUser, @Args('_id')_id: string){
        return await this.menuService.deleteMenu(currentUser._id, _id);
    }

    @Mutation(() => String)
    async deleteDishInMenu(@Context('currentUser')currentUser, @Args('menuId')menuId: string, @Args('dishId')dishId: string){
        return await this.menuService.deleteDishInMenu(currentUser._id, menuId, dishId)
    }
}
