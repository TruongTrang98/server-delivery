import { Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { MongoRepository } from 'typeorm'
import { ApolloError } from 'apollo-server-core'
import { Shop, ShopInput, ShopResponse } from './shop.entity'
import * as diacritic from 'diacritic'
import { AccountService } from '../account/account.service'
import { ImageService } from '../image/image.service'
import { ImageInput } from '../image/image.entity'

@Injectable()
export class ShopService {
	constructor(
		@InjectRepository(Shop)
        private readonly shopRepository: MongoRepository<Shop>,
        private readonly accountService: AccountService,
        private readonly imageService: ImageService
    ) {}

    async findById (_id: string): Promise<ShopResponse> {
        const shopResponse = await this.shopRepository.aggregate([
            {
                $match: {
                    _id
                }
            },
            {
                $lookup: {
                    from: "account",
                    localField: "accountId",
                    foreignField: "_id",
                    as: "account"
                }
            },
            {
                $unwind : "$account"
            }
        ]).toArray()
        return shopResponse[0]
    }

    async findByAccountId(accountId: string): Promise<Shop>{
        const shopResponse = await this.shopRepository.aggregate([
            {
                $match: {
                    accountId
                }
            },
            {
                $lookup: {
                    from: "account",
                    localField: "accountId",
                    foreignField: "_id",
                    as: "account"
                }
            },
            {
                $unwind : "$account"
            }
        ]).toArray()
        return shopResponse[0]
    }

    // async findAll(): Promise<ShopResponse[]> {
    //     const shopResponse = await this.shopRepository.aggregate([
    //         {
    //             $lookup: {
    //                 from: "account",
    //                 localField: "accountId",
    //                 foreignField: "_id",
    //                 as: "account"
    //               }
    //         },
    //         {
    //             $unwind : "$account"
    //         }
    //     ]).toArray()
    //     return shopResponse
    // }

    async findAll(txtSearch: string, txtFilter: string, offset: number, limit: number): Promise<ShopResponse[]> {
        const shopsResponse = await this.shopRepository.aggregate([
            {
                $lookup: {
                    from: "account",
                    localField: "accountId",
                    foreignField: "_id",
                    as: "account"
                }
            },
            {
                $unwind : "$account"
            },
            {
                $match: {
                    $or: [{nameStr: { $regex: !!txtSearch ? `${txtSearch}` : `` }}, {dishesStr: { $regex: !!txtSearch ? `${txtSearch}` : `` }}, {addressStr: { $regex: !!txtSearch ? `${txtSearch}` : `` }}],
                    addressStr: { $regex: !!txtFilter ? `${txtFilter}` : `` },
                    "account.isLocked": false
                }
            },
            {
                $skip: offset
            },
            {
                $limit: limit
            },
            { $sort : { createdAt : -1 } }

        ]).toArray()
        return shopsResponse
    }

    async findAllForAdmin(txtSearch: string, txtFilter: string, offset: number, limit: number): Promise<ShopResponse[]> {
        const shopsResponse = await this.shopRepository.aggregate([
            {
                $lookup: {
                    from: "account",
                    localField: "accountId",
                    foreignField: "_id",
                    as: "account"
                }
            },
            {
                $unwind : "$account"
            },
            {
                $match: {
                    name: { $regex: !!txtSearch ? `${txtSearch}` : `` },
                    // nameStr:  { $regex: !!txtSearch ? `${txtSearch}` : `` },
                    $or: (txtFilter === 'all' || !txtFilter) ? [{ "account.isLocked": false }, { "account.isLocked": true }] 
						: txtFilter === 'isLocked' ? [{ "account.isLocked": true }]
						: [{ "account.isLocked": false }]
                }
            },
            {
                $skip: offset
            },
            {
                $limit: limit
            }
        ]).toArray()
        return shopsResponse
    }

    async numberOfShopForAdmin (txtSearch: string, txtFilter: string): Promise<number> {
        return (await this.shopRepository.aggregate([
			{
				$lookup: {
					from: "account",
					localField: "accountId",
					foreignField: "_id",
					as: "account"
				}
			},
			{
				$unwind: "$account"
			},
			{
				$match: {
					name: { $regex: !!txtSearch ? `${txtSearch}` : `` },
					$or: (txtFilter === 'all' || !txtFilter) ? [{ "account.isLocked": false }, { "account.isLocked": true }] 
						: txtFilter === 'isLocked' ? [{ "account.isLocked": true }]
						: [{ "account.isLocked": false }]
				}
			}
		]).toArray()).length
    }



    async shopsForUser(txtFilter: string, limit: number): Promise<ShopResponse[]> {
        const shopResponse = await this.shopRepository.aggregate([
            {
                $lookup: {
                    from: "account",
                    localField: "accountId",
                    foreignField: "_id",
                    as: "account"
                  }
            },
            { $sort : { updatedAt : -1 } },
            {
                $unwind : "$account"
            },
            {
                $match: {
                    'account.isLocked': false,                    
                    addressStr: { $regex: !!txtFilter ? `${txtFilter}` : `` }
                }
            },
            {
                $limit: limit
            }
            // {
            //     $match: {
            //         isLocked: { $eq: false }
            //     }
            // }
        ]).toArray()
        return shopResponse
    }

    async numberOfShop (txtSearch: string, txtFilter: string): Promise<number> {
        return await this.shopRepository.count({ 
            // nameStr:  { $regex: !!txtSearch ? `${txtSearch}` : `` },
            // addressStr: { $regex: !!txtFilter ? `${txtFilter}` : `` }
            $or: [{nameStr: { $regex: !!txtSearch ? `${txtSearch}` : `` }}, {dishesStr: { $regex: !!txtSearch ? `${txtSearch}` : `` }}, {addressStr: { $regex: !!txtSearch ? `${txtSearch}` : `` }}],
            // nameStr:  { $regex: !!txtSearch ? `${txtSearch}` : `` },
            addressStr: { $regex: !!txtFilter ? `${txtFilter}` : `` }
        })
    }

    async create (input: ShopInput): Promise<String> {
        try {
            const { name, phoneNumber, email, address, password, image } = input
            const exitedShopEmail = await this.shopRepository.findOne({email})
            const exitedShopPhone = await this.shopRepository.findOne({phoneNumber})

            if(exitedShopEmail){
                return '410'
            }
            if( exitedShopPhone) {
                return '411'
            }

            const account = await this.accountService.create({username: email, password, role: "partner"})
            if(account){
                // const file = image.data.replace(/^data:image\/(png|gif|jpeg);base64,/, '')
                // await fs.writeFile(
                //     `static/images/${account._id}.${image.type.split('/')[1]}`,
                //     file,
                //     'base64',
                //     err => {
                //     if (err) {
                //         console.log(err)
                //     }
                //     }
                // )
                const shop = new Shop ()
                shop.name = name
                shop.phoneNumber = phoneNumber
                shop.email = email
                shop.address = address
                shop.imageUrl = `${account._id}.${image.type.split('/')[1]}`
                shop.accountId = account._id
                await this.shopRepository.save(shop)
                await this.imageService.updateImage(account._id, image)
                return '200'
            }
        } catch (error){
            throw new ApolloError(
                error,
                error.extensions ? error.extensions.code : '403'
              )
        }
    }

    async updateInfo (_id: string, input: ShopInput): Promise<String> {
        try {
            //const { name, phoneNumber, email, address, password } = input
            delete input.image
            delete input.password
            const exitedShop = await this.shopRepository.findOne({_id})
            if(!exitedShop){
                return '409'
            }
            await this.shopRepository.save({
                _id, ...input, 
                imageUrl: exitedShop.imageUrl, 
                nameStr: diacritic.clean(input.name).toLowerCase(),
                addressStr: diacritic.clean(input.address).toLowerCase()
            })
            return '200'

        } catch (error){
            throw new ApolloError(
                error,
                error.extensions ? error.extensions.code : '403'
              )
        }
    }

    async delete (_id: string): Promise<String> {
        try {
            const exitedShop = await this.shopRepository.findOne({_id})
            if(!exitedShop){
                return '409'
            }
            await this.shopRepository.deleteOne({_id})
            return '200'

        } catch (error){
            throw new ApolloError(
                error,
                error.extensions ? error.extensions.code : '403'
              )
        }
    }

    async updateAvatar(_id: string, image: ImageInput): Promise<String> {
		try {
			let shop = await this.shopRepository.findOne({ _id })
			if(!shop) {
				return '409'
            }
            console.log(shop);
			shop.imageUrl = `${_id}.${image.type.split('/')[1]}`
			await this.shopRepository.save(shop)
			await this.imageService.updateImage(_id, image)
			return '200'
		} catch (error) {
			throw new ApolloError(
				error,
				error.extensions ? error.extensions.code : '403'
			)
		}
	}

}