import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Shop } from './shop.entity';
import { ShopService } from './shop.service';
import { ShopResolver } from './shop.resolver';
import { AccountModule } from '../account/account.module';
import { ImageModule } from '../image/image.module';

@Module({
    imports: [TypeOrmModule.forFeature([Shop]), AccountModule, ImageModule],
	providers: [ShopService, ShopResolver],
    exports: [ShopService]
})
export class ShopModule{}
