import {
	Entity,
	ObjectIdColumn,
    Column,
    CreateDateColumn,
	UpdateDateColumn,
	BeforeInsert,
} from 'typeorm'
import * as uuid from 'uuid'
import * as diacritic from 'diacritic'
import { Account } from '../account/account.entity'
import { ImageInput } from '../image/image.entity'

@Entity()
export class Shop {
    @ObjectIdColumn()
    _id: string

    @Column()
    name: string

    @Column()
    nameStr: string

    @Column()
    dishesStr: string

    @Column()
    address: string

    @Column()
    addressStr: string

    @Column()
    phoneNumber: string

    @Column()
    email: string

    @Column()
    imageUrl: string

    @Column()
    accountId: string

    @Column()
    menus: string[]

    @Column()
    isActive: boolean

    @CreateDateColumn()
    createdAt: string

    @UpdateDateColumn()
    updatedAt: string

	@BeforeInsert()
	async beforeInsert() {
        const uid = await uuid.v4()
        this._id = uid
        this.nameStr = diacritic.clean(this.name).toLowerCase()
        this.addressStr = diacritic.clean(this.address).toLowerCase()
        this.dishesStr = ''
        this.isActive = true
        this.menus = []
	}
}

export class ShopInput {
    name: string
    address: string
    phoneNumber: string
    email: string
    image: ImageInput
    password: string
}

export class ShopResponse {
    _id: string
    name: string
    address: string
    phoneNumber: string
    email: string
    imageUrl: string
    account: Account
    menus: string []
    isActive: boolean
    createdAt: string
    updatedAt: string
}
