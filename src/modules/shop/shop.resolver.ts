import {
	Resolver,
	Query,
	Mutation,
	Args,
    Context,
} from '@nestjs/graphql'
import { ShopService } from './shop.service'
import { Shop, ShopInput, ShopResponse } from './shop.entity'
import { ImageInput } from '../image/image.entity'

import * as diacritic from 'diacritic'

@Resolver('Shop')
export class ShopResolver {
	constructor(
		private readonly shopService: ShopService
    ) {}

    @Query(() => ShopResponse)
    async meShop(@Context('currentUser')currentUser) {
        const res = await this.shopService.findById(currentUser._id);
        return res;
    }

    @Query(() => [ShopResponse])
    async shops (
        @Args('txtSearch') txtSearch: string, 
        @Args('txtFilter') txtFilter: string, 
        @Args('offset') offset: number, 
        @Args('limit') limit: number
        ) {
       // return await this.shopService.findAll()
       return await this.shopService.findAll(txtSearch, txtFilter, offset, limit)
    }

    @Query(() => [ShopResponse])
    async shopsForAdmin (
        @Args('txtSearch') txtSearch: string, 
        @Args('txtFilter') txtFilter: string, 
        @Args('offset') offset: number, 
        @Args('limit') limit: number
        ) {
       // return await this.shopService.findAll()
       return await this.shopService.findAllForAdmin(txtSearch, txtFilter, offset, limit)
    }

    @Query(() => Number)
    async numberOfShopForAdmin (@Args('txtSearch') txtSearch: string, @Args('txtFilter') txtFilter: string) {
        return await this.shopService.numberOfShopForAdmin(txtSearch, txtFilter)
    }


    @Query(() => [ShopResponse])
    async shopsForUser (@Args('txtFilter') txtFilter: string, @Args('limit') limit: number) {
        return await this.shopService.shopsForUser(txtFilter, limit)
    }

    @Query(() => ShopResponse)
    async shopById (@Args('_id')_id: string) {
        return await this.shopService.findById(_id)
    }

    @Query(() => Number)
    async numberOfShop (@Args('txtSearch') txtSearch: string, @Args('txtFilter') txtFilter: string) {
        return await this.shopService.numberOfShop(txtSearch, txtFilter)
    }

    @Mutation(() => String)
    async createShop (@Args('input')input: ShopInput) {
        return await this.shopService.create(input)
    }

    @Mutation (() => String)
    async updateShopInfo (@Args('_id')_id: string, @Args('input')input: ShopInput) {
        return await this.shopService.updateInfo(_id, input)
    }

    @Mutation(() => String)
	async updateShopAvatar(
		@Args('_id') _id: string,
		@Args('image') image: ImageInput
	) {
		return await this.shopService.updateAvatar(_id, image)
	}

    @Mutation (() => String)
    async deleteShop(@Args('_id')_id: string){
        return await this.shopService.delete(_id)
    }

    @Query(() => String)
    async test(@Args('input') input: string) {
        return diacritic.clean(input).toLowerCase()
    }
}
