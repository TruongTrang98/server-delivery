import * as dotenv from 'dotenv'
dotenv.config()

// COMPLETE:
const config = {
	development: {
        domain: 'localhost',
		port: process.env.PORT,
		end_point: 'graphqldelivery',
		orm: {
			type: 'mongodb',
			url: 'mongodb://dev:dev123456@ds217078.mlab.com:17078/delivery',
			// url: 'mongodb://localhost:27017',
			// host: 'localhost',
			// port: process.env.MONGO_PORT,
			// username: '',
			// password: '',
			database: 'delivery',
			useNewUrlParser: true,
			useUnifiedTopology: true,
		}
	},
	production: {
        domain: 'localhost',
		port: process.env.PORT,
		end_point: 'graphqldelivery',
		orm: {
			type: 'mongodb',
			url: 'mongodb://dev:dev123456@ds217078.mlab.com:17078/delivery',
			host: 'localhost',
			port: process.env.MONGO_PORT,
			//port: 27017,
			// username: '',
			// password: '',
			database: 'delivery',
			useNewUrlParser: true,
			useUnifiedTopology: true,
		}
	}
}

export default config[process.env.NODE_ENV || 'development']
