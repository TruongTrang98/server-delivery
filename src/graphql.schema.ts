
/** ------------------------------------------------------
 * THIS FILE WAS AUTOMATICALLY GENERATED (DO NOT MODIFY)
 * -------------------------------------------------------
 */

/* tslint:disable */
export class AccountInput {
    username: string;
    password: string;
    role: string;
}

export class ChangePasswordInput {
    oldPassword: string;
    newPassword: string;
}

export class CouponInput {
    content: string;
    percent: string;
    dishes?: string[];
}

export class DishInfoInput {
    _id: string;
    name: string;
    count: number;
    price: string;
}

export class DishInput {
    name: string;
    image?: ImageInput;
    price: string;
}

export class ImageInput {
    data: string;
    type: string;
}

export class LoginInput {
    username: string;
    password: string;
}

export class MenuInput {
    name: string;
    dishes?: string[];
}

export class OrderInput {
    shopId: string;
    userId: string;
    name: string;
    phoneNumber: string;
    address: string;
    costShip: string;
    costDishes: string;
    dishes: DishInfoInput[];
}

export class ShipperInput {
    name: string;
    address: string;
    image: ImageInput;
    gender: string;
    phoneNumber: string;
    cmnd: string;
    email: string;
    password: string;
}

export class ShopInput {
    name: string;
    address: string;
    phoneNumber: string;
    email: string;
    image: ImageInput;
    password: string;
}

export class UserInput {
    name: string;
    image?: ImageInput;
    gender: string;
    email: string;
    phoneNumber: string;
    address: string;
    password: string;
}

export class Account {
    _id: string;
    username: string;
    password: string;
    roleId: string;
    isLocked: boolean;
    reason: string;
    isActive: boolean;
    createdAt: string;
    updatedAt: string;
}

export class Coupon {
    _id: string;
    content: string;
    percent: string;
    dishes: string[];
    shopId: string;
    createdAt: string;
    updatedAt: string;
}

export class CouponResponse {
    _id: string;
    content: string;
    percent: string;
    dishes?: Dish[];
    shop: Shop;
    createdAt: string;
    updatedAt: string;
}

export class Dish {
    _id: string;
    name: string;
    imageUrl: string;
    price: string;
    couponId?: string;
    shopId: string;
    isActive: boolean;
    createdAt: string;
    updatedAt: string;
}

export class DishInfo {
    _id: string;
    name: string;
    count: number;
    price: string;
}

export class DishResponse {
    _id: string;
    name: string;
    imageUrl?: string;
    price: string;
    coupon?: Coupon;
    shop: Shop;
    isActive: boolean;
    createdAt: string;
    updatedAt: string;
}

export class LoginResponse {
    token: string;
    profile: string;
}

export class Menu {
    _id: string;
    name: string;
    shopId: string;
    dishes: string[];
    isActive: boolean;
    createdAt: string;
    updatedAt: string;
}

export class MenuResponse {
    _id: string;
    name: string;
    shop: Shop;
    dishes: DishResponse[];
    isActive: boolean;
    createdAt: string;
    updatedAt: string;
}

export abstract class IMutation {
    abstract createAccount(input: AccountInput): Account | Promise<Account>;

    abstract adminLogin(input: LoginInput): LoginResponse | Promise<LoginResponse>;

    abstract shopLogin(input: LoginInput): LoginResponse | Promise<LoginResponse>;

    abstract userLogin(input: LoginInput): LoginResponse | Promise<LoginResponse>;

    abstract shipperLogin(input: LoginInput): LoginResponse | Promise<LoginResponse>;

    abstract lockAndUnlockAccount(_id: string, reason: string): string | Promise<string>;

    abstract changePassword(input: ChangePasswordInput): string | Promise<string>;

    abstract createCoupon(input?: CouponInput): string | Promise<string>;

    abstract updateCoupon(_id: string, input: CouponInput): string | Promise<string>;

    abstract deleteCoupon(_id?: string): string | Promise<string>;

    abstract deleteDishInCoupon(couponId: string, dishId: string): string | Promise<string>;

    abstract createDish(input: DishInput): string | Promise<string>;

    abstract updateDish(_id: string, input: DishInput): string | Promise<string>;

    abstract deleteDish(_id: string): string | Promise<string>;

    abstract updateImage(_id: string, image: ImageInput): string | Promise<string>;

    abstract createMenu(input: MenuInput): string | Promise<string>;

    abstract updateMenu(_id: string, input: MenuInput): string | Promise<string>;

    abstract deleteMenu(_id?: string): string | Promise<string>;

    abstract deleteDishInMenu(menuId: string, dishId: string): string | Promise<string>;

    abstract book(input: OrderInput): OrderResponse | Promise<OrderResponse>;

    abstract changeStatus(_id: string, status: string): string | Promise<string>;

    abstract shipperBookOrder(_id: string): string | Promise<string>;

    abstract create(code: string): Role | Promise<Role>;

    abstract createShipper(input: ShipperInput): string | Promise<string>;

    abstract updateShipperInfo(_id?: string, input?: ShipperInput): string | Promise<string>;

    abstract updateShipperAvatar(_id: string, image: ImageInput): string | Promise<string>;

    abstract createShop(input: ShopInput): string | Promise<string>;

    abstract updateShopInfo(_id: string, input: ShopInput): string | Promise<string>;

    abstract updateShopAvatar(_id: string, image: ImageInput): string | Promise<string>;

    abstract deleteShop(_id: string): string | Promise<string>;

    abstract createUser(input: UserInput): string | Promise<string>;

    abstract updateUserInfo(_id: string, input: UserInput): string | Promise<string>;

    abstract updateUserAvatar(_id: string, image: ImageInput): string | Promise<string>;

    abstract user(_id?: string): User | Promise<User>;
}

export class Order {
    _id: string;
    name: string;
    phoneNumber: string;
    address: string;
    costShip: string;
    costDishes: string;
    shopId: string;
    userId: string;
    shipperId?: string;
    dishes: DishInfo[];
    status: string;
    reason?: string;
    createdAt: string;
    updatedAt: string;
}

export class OrderResponse {
    _id: string;
    shop: Shop;
    user: User;
    name: string;
    address: string;
    phoneNumber: string;
    costDishes: string;
    costShip: string;
    shipperId?: string;
    dishes: DishInfo[];
    status: string;
    reason?: string;
    createdAt: string;
    updatedAt: string;
}

export abstract class IQuery {
    abstract accounts(): Account[] | Promise<Account[]>;

    abstract coupons(): CouponResponse[] | Promise<CouponResponse[]>;

    abstract couponsForUser(): CouponResponse[] | Promise<CouponResponse[]>;

    abstract coupon(_id: string): CouponResponse | Promise<CouponResponse>;

    abstract dishes(): DishResponse[] | Promise<DishResponse[]>;

    abstract dishesForCoupon(): DishResponse[] | Promise<DishResponse[]>;

    abstract dishesForUser(): DishResponse[] | Promise<DishResponse[]>;

    abstract dishById(_id: string): DishResponse | Promise<DishResponse>;

    abstract dishesWithoutCoupon(): DishResponse[] | Promise<DishResponse[]>;

    abstract menu(_id: string): MenuResponse | Promise<MenuResponse>;

    abstract menus(): MenuResponse[] | Promise<MenuResponse[]>;

    abstract menuByShopId(shopId: string): MenuResponse[] | Promise<MenuResponse[]>;

    abstract ordersByUser(_id: string): OrderResponse[] | Promise<OrderResponse[]>;

    abstract ordersPending(): OrderResponse[] | Promise<OrderResponse[]>;

    abstract ordersForShopStatistics(): OrderResponse[] | Promise<OrderResponse[]>;

    abstract ordersReceivedAndShipperReceived(idSearch?: string): OrderResponse[] | Promise<OrderResponse[]>;

    abstract ordersShopReceived(): OrderResponse[] | Promise<OrderResponse[]>;

    abstract ordersShopDone(idSearch?: string): OrderResponse[] | Promise<OrderResponse[]>;

    abstract orderShipperBooked(): OrderResponse | Promise<OrderResponse>;

    abstract shipperHistoryOrders(): OrderResponse[] | Promise<OrderResponse[]>;

    abstract orderByUser(): OrderResponse[] | Promise<OrderResponse[]>;

    abstract role(_id: string): Role | Promise<Role>;

    abstract meShipper(): ShipperResponse | Promise<ShipperResponse>;

    abstract shippers(offset: number, limit: number, txtSearch?: string, txtFilter?: string): ShipperResponse[] | Promise<ShipperResponse[]>;

    abstract numberOfShipper(txtSearch?: string, txtFilter?: string): number | Promise<number>;

    abstract shipperById(_id: string): ShipperResponse | Promise<ShipperResponse>;

    abstract shops(offset: number, limit: number, txtSearch?: string, txtFilter?: string): ShopResponse[] | Promise<ShopResponse[]>;

    abstract numberOfShop(txtSearch?: string, txtFilter?: string): number | Promise<number>;

    abstract shopsForUser(limit: number, txtFilter?: string): ShopResponse[] | Promise<ShopResponse[]>;

    abstract shopById(_id: string): ShopResponse | Promise<ShopResponse>;

    abstract test(input: string): string | Promise<string>;

    abstract meShop(): ShopResponse | Promise<ShopResponse>;

    abstract shopsForAdmin(offset: number, limit: number, txtSearch?: string, txtFilter?: string): ShopResponse[] | Promise<ShopResponse[]>;

    abstract numberOfShopForAdmin(txtSearch?: string, txtFilter?: string): number | Promise<number>;

    abstract userById(_id: string): UserResponse | Promise<UserResponse>;

    abstract users(offset: number, limit: number, txtSearch?: string, txtFilter?: string): UserResponse[] | Promise<UserResponse[]>;

    abstract me(): UserResponse | Promise<UserResponse>;

    abstract numberOfUser(txtSearch?: string, txtFilter?: string): number | Promise<number>;
}

export class Role {
    _id: string;
    code: string;
}

export class Shipper {
    _id: string;
    name: string;
    address: string;
    imageUrl: string;
    gender: string;
    email: string;
    phoneNumber: string;
    cmnd: string;
    accountId: string;
    isActive: boolean;
    createdAt: string;
    updatedAt: string;
}

export class ShipperResponse {
    _id: string;
    name: string;
    address: string;
    imageUrl: string;
    gender: string;
    email: string;
    phoneNumber: string;
    cmnd: string;
    account: Account;
    isActive: boolean;
    createdAt: string;
    updatedAt: string;
}

export class Shop {
    _id: string;
    name: string;
    nameStr: string;
    dishesStr: string;
    address: string;
    addressStr: string;
    phoneNumber: string;
    email: string;
    imageUrl: string;
    accountId: string;
    menus?: string[];
    isActive: boolean;
    createdAt: string;
    updatedAt: string;
}

export class ShopResponse {
    _id: string;
    name: string;
    address: string;
    phoneNumber: string;
    email: string;
    imageUrl: string;
    account: Account;
    menus?: string[];
    isActive: boolean;
    createdAt: string;
    updatedAt: string;
    dishesStr?: string;
    addressStr?: string;
    nameStr?: string;
}

export abstract class ISubscription {
    abstract ordersCreated(): OrderResponse[] | Promise<OrderResponse[]>;

    abstract ordersShopPublish(): OrderResponse[] | Promise<OrderResponse[]>;
}

export class User {
    _id: string;
    name: string;
    imageUrl?: string;
    gender: string;
    email: string;
    phoneNumber: string;
    address: string;
    accountId: string;
    isActive: boolean;
    createdAt: string;
    updatedAt: string;
}

export class UserResponse {
    _id: string;
    name: string;
    imageUrl?: string;
    gender: string;
    email: string;
    phoneNumber: string;
    address: string;
    account: Account;
    isActive: boolean;
    createdAt: string;
    updatedAt: string;
}
