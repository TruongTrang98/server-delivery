import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import { LoggerService } from '@nestjs/common'
import { AppModule } from './app.module';
import { Logger } from '@nestjs/common'
import * as helmet from 'helmet'
import * as path from 'path';
import * as bodyParser from 'body-parser'
import { express as createSchema } from 'graphql-voyager/middleware'

import config from './config.env'

import { createConnection } from 'typeorm'

createConnection(config.orm)
	.then(connection => Logger.log(`☁️  Database connected`, 'TypeORM'))
	.catch(error => Logger.log(`❌  Database connect error`, 'TypeORM'))

  export class MyLogger implements LoggerService {
    log(message: string) {
      // console.log(message)
    }
    error(message: string, trace: string) {
      console.log('error', message, trace)
    }
    warn(message: string) {
      console.log('warn', message)
    }
    debug(message: string) {
    }
    verbose(message: string) {
    }
  }

const domain = config.domain
const port = config.port
const end_point = config.end_point
declare const module: any

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule, {
		// cors: true,
		// DONE:
		logger: new MyLogger()
	});

  app.use(helmet());
  app.enableCors({
    origin: '*',
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS',
    preflightContinue: true,
    optionsSuccessStatus: 204,
    credentials: true,
  });
  app.useStaticAssets(path.join(__dirname, '../static'));
console.log(path.join(__dirname, '../static'))
  app.use(bodyParser.json({limit: '50mb'}));
  app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

  if (process.env.NODE_ENV !== 'production') {
    app.use('/schema', createSchema({ endpointUrl: '/graphqldelivery' }))
  }

  if (module.hot) {
    module.hot.accept()
    module.hot.dispose(() => app.close())
  }

  await app.listen(port);

  Logger.log(
		`🚀  Server ready at http://${domain}:${port}/${end_point}`,
		'Bootstrap'
	)
	Logger.log(
		`🚀  Subscriptions ready at ws://${domain}:${port}/${end_point}`,
		'Bootstrap'
	)
}
bootstrap();
