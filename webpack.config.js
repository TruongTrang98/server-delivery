const webpack = require('webpack')
const { join, resolve } = require('path')
const nodeExternals = require('webpack-node-externals')

module.exports = {
  entry: ['webpack/hot/poll?100', './src/main.ts'],
  watch: true,
  target: 'node',
  externals: [
    nodeExternals({
      whitelist: ['webpack/hot/poll?100']
    })
  ],
  node: {
    __dirname: false,
    __filename: false
  },
  module: {
    rules: [
      {
        test: /.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/
      },
      { test: /\.(png|jpe?g|svg|)$/, use: { loader: 'url-loader?limit=90000' }}
    ]
  },
  mode: 'development',
  resolve: {
    extensions: ['.tsx', '.ts', '.js'],
    alias: {
      '@src': resolve(__dirname, 'src/'),
      '@modules': resolve(__dirname, 'src/modules/'),
    }
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin()
  ],
  output: {
    path: join(__dirname, '.hot'),
    filename: 'server.js'
  }
}
